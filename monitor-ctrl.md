
# Turn on monitor from Rasbperry pi

## Installation

```bash
sudo apt-get install cec-utils
```

## Interactive commands

```
cec-client # Start interactive client
h # Print help
lad # list active devices
name 0 # Prints the device name
pow 0 # Get the current monitor status
on 0 # Turn on monitor with
standby 1 # Turn off monitor with
```

## One shot command

```bash
echo "standby 0" | cec-client -s
```
