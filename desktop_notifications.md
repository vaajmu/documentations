# Try to show desktop notifications with notify-send

## Environment

Debian testing (from Debian stretch)

21st december 2017

## First tests

Command `notify-send` doesn't work.

Start `busctl --user monitor` and try again. It shows errors

[Arch wiki page on desktop
notifications](https://wiki.archlinux.org/index.php/Desktop_notifications) seems
good.

## Install an other notification server

### Try with `dunst`

```bash
cd ~/workspace
git clone https://github.com/dunst-project/dunst.git
cd dunst
less README.md
sudo apt-get install libxinerama1 libxrandr2 libxss1 libxdg-basedir1 cairo-5c libgtk3.0-cil-dev
make
```

Error :

```
Package gdk-3.0 was not found in the pkg-config search path.
Perhaps you should add the directory containing `gdk-3.0.pc'
to the PKG_CONFIG_PATH environment variable
No package 'gdk-3.0' found
Package pangocairo was not found in the pkg-config search path.
Perhaps you should add the directory containing `pangocairo.pc'
to the PKG_CONFIG_PATH environment variable
No package 'pangocairo' found
Package libxdg-basedir was not found in the pkg-config search path.
Perhaps you should add the directory containing `libxdg-basedir.pc'
to the PKG_CONFIG_PATH environment variable
No package 'libxdg-basedir' found
Package gdk-3.0 was not found in the pkg-config search path.
Perhaps you should add the directory containing `gdk-3.0.pc'
to the PKG_CONFIG_PATH environment variable
No package 'gdk-3.0' found
Package pangocairo was not found in the pkg-config search path.
Perhaps you should add the directory containing `pangocairo.pc'
to the PKG_CONFIG_PATH environment variable
No package 'pangocairo' found
Package libxdg-basedir was not found in the pkg-config search path.
Perhaps you should add the directory containing `libxdg-basedir.pc'
to the PKG_CONFIG_PATH environment variable
No package 'libxdg-basedir' found
Makefile:16: *** "pkg-config failed!".  Stop.
```

dunst wiki shows a list of dependencies for Debian Jessy

```bash
sudo apt-get install libdbus-1-dev libx11-dev libxinerama-dev libxrandr-dev libxss-dev libglib2.0-dev libpango1.0-dev libgtk-3-dev libxdg-basedir-dev
make
```

Success !

```bash
./dunst -h
./dunst
```

From another terminal :

```bash
notify-send hello
```

It works !!

Notification is on the top left corner of the screen.

Configure dunst with the default configuration file :

```bash
mkdir -p ~/.config/dunst
cp dunstrc ~/.config/dunst/
```

### Install dunst on Ubuntu

```bash
sudo apt-get install dunst
sudo emacs /usr/share/dbus-1/services/org.freedesktop.Notifications.service # Replace Exec rule with '/usr/bin/dunst'
```

Then logout and back in. It fails. Start another X session with the first one
still alive. It works on both X. Close the second X, it doesn't work anymore on
the first X.

```bash
sudo apt-get purge dunst
sudo apt-get install libdbus-1-dev libx11-dev libxinerama-dev libxrandr-dev libxss-dev libglib2.0-dev libpango1.0-dev libgtk-3-dev libxdg-basedir-dev
sudo apt-get install dbus-x11 dbus-user-session
git clone https://github.com/dunst-project/dunst.git
cd dunst
make
sudo make install
sudo apt-get install d-feet
d-feet
which dunst
sudo emacs /usr/share/dbus-1/services/org.freedesktop.Notifications.service # Update dunst path
dunst # Works
```

Add dunst in `~/.xinitrc`

## Add SMS support

Send SMS when a notification is not removed soon enough

Install `dunst` to be linked with `org.freedesktop.Notifications` to avoid the
following error message :

```
Type=error  Endian=l  Flags=1  Version=1  Priority=0 Cookie=5  ReplyCookie=4
Sender=org.freedesktop.DBus  Destination=:1.835
ErrorName=org.freedesktop.DBus.Error.ServiceUnknown  ErrorMessage="The name org.freedesktop.Notifications was not provided by any .service files"
MESSAGE "s" {
STRING "The name org.freedesktop.Notifications was not provided by any .service files";
};
```

```bash
sudo make install
```

Check if the error is still there

```bash
busctl --user monitor
```

```bash
notify-send "hello"
```

No error message !

Stop `dust` process and try again

```
Type=error  Endian=l  Flags=1  Version=1  Priority=0 Cookie=5  ReplyCookie=4
Sender=org.freedesktop.DBus  Destination=:1.844
ErrorName=org.freedesktop.systemd1.NoSuchUnit  ErrorMessage="Unit dunst.service not found."
MESSAGE "s" {
STRING "Unit dunst.service not found.";
};
```

```bash
systemctl --user daemon-reload
```

Try again. There is no error message but notify-send command doesn't end and
there is no notification. Last traces from command `bustcl --user monitor` is :

```
Type=method_call  Endian=l  Flags=0  Version=1  Priority=0 Cookie=4
Sender=:1.909  Destination=org.freedesktop.DBus  Path=/org/freedesktop/DBus  Interface=org.freedesktop.DBus  Member=StartServiceByName
UniqueName=:1.909
MESSAGE "su" {
STRING "org.freedesktop.Notifications";
UINT32 0;
};
```

```bash
cat /usr/local/lib/systemd/user/dunst.service
```

Maybe `dunst` must be started, which is consistent with the
`WantedBy=default.target`

```bash
systemctl --user list-units
```

`dunst.service` failed

```bash
systemctl --user status dunst.service
```

failed a few minutes ago. Try again notify-send and check if the last failure is
closer.

> It does !

```bash
systemctl --user start dunst.service
sudo journalctl -xe
```

```
Dec 22 15:57:57 anloh dunst[25571]: cannot open display
Dec 22 15:57:57 anloh systemd[997]: dunst.service: Main process exited, code=exited, status=1/FAILURE
Dec 22 15:57:57 anloh systemd[997]: dunst.service: Failed with result 'exit-code'.
Dec 22 15:57:57 anloh systemd[997]: Failed to start Dunst notification daemon.
```

It doesn't work because systemd is not supposed to start a GUI app (and dunst
show notifications). Maybe xsession is better for that.

Maybe it is, but dunst project gave a systemd script, so it is intended to be
started with systemd. The FAQ contains that error. Add `systemctl --user
set-environment DISPLAY=$DISPLAY` in ~/.xinitrc. It works only with one session.

Try to start a session, stop dunst service and start a dunst **process**. Try
again in another session and see if notifications are displayed in the
respective session.

Dunst systemd service is automatically started when notify-send is called
because of the dunst service configuration (don't know what part
exactly). Uninstall dunst service configuration and try again :

```bash
systemctl --user cat dunst.service
```

dunst service is not linked to graphical-session.target because the service file
is installed with `install` command but `systemctl --user enable` is not used

```bash
make -n install
sudo rm /usr/local/share/dbus-1/services/org.knopwob.dunst.service
systemctl --user stop dunst.service
systemctl --user daemon-reload
ps -e | grep dunst # returns nothing
notify-send hello # shows error from busctl and doesn't display a notification
```

Try again to start dunst from 2 X sessions and send a different notification
from each session.

Fail to start second dunst process : `Cannot acquire
'org.freedesktop.Notifications': Name is acquired by PID '15816'.`

Install again dunst to restore org.knopwob.dunst.service file (`sudo make install`)

From [desktop notifications specifications
](https://people.gnome.org/~mccann/docs/notification-spec/notification-spec-latest.html)
the notifications daemon is a `session-scoped` service. Maybe the problem is due
to xsession not creating a new session.

### Suggestions

* `Free` phone company allow to send an SMS with a HTTPS POST request
* Trigger script if notification isn't removed
  * Give a hint to `notify-send` named `x-anloh-action-timeout`
  * Use scripts in rules after a delay
  * `dunst` send a dbus event when a notification is removed, try to catch it

### Try catching dunst `d-bus` event when notification is closed

On the first hand `notify-send` get an id on `d-bus`, send the notification,
drop the id and exit. `dunst` on the other hand receive the `d-bus` notification
and send a response to the `notify-send` id. It might be better to get
notify-send sources and wait for the answer in the notify-send process

Download notify-send sources. Search for `notify-send debian package` on
`Google`, `packages.debian.org` indicate notify-send command is part of
`libnotify-bin` package

```bash
cd ~/workspace
apt-get source libnotify-bin
cd libnotify-0.7.7/
./configure
make # returned 1 !
./tools/notify-send "hello" # works
make clean # ./tools/notify-send is removed so it WAS generated
make # still return 1
```

It may be better to make a program who spy the bus than patch libnotify. It
allow user to install it everywhere without modifying the notification tools nor
the notification daemon.

### Make a program to spy notifications

Search for a DBus API

GLib Objects looks nice :
https://developer.gnome.org/glib/2.54/glib-Singly-Linked-Lists.html#g-slist-insert-sorted

## Notes

We may list notifications daemons with the command
`gdbus call --session --dest org.freedesktop.DBus --object-path
/org/freedesktop/Dbus --method org.freedesktop.DBus.ListNames`

[Desktop notifications article on
ArchWiki](https://wiki.archlinux.org/index.php/Desktop_notifications) show how
to send a notification from C.

`xev` command can show which key is used

`xmodmap -pke` list all keyboard keys


