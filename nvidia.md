# How to install graphic card drivers

```bash
apt-cache policy nvidia-driver nvidia-smi
```

```
nvidia-driver:
  Installed: (none)
  Candidate: 470.74-1
  Version table:
     470.74-1 500
        500 http://debian.proxad.net/debian testing/non-free amd64 Packages
nvidia-smi:
  Installed: (none)
  Candidate: 470.74-1
  Version table:
     470.74-1 500
        500 http://debian.proxad.net/debian testing/non-free amd64 Packages
```

```
lshw -C video
```

```
WARNING: you should run this program as super-user.
  *-display
       description: VGA compatible controller
       product: HD Graphics 5500
       vendor: Intel Corporation
       physical id: 2
       bus info: pci@0000:00:02.0
       logical name: /dev/fb0
       version: 09
       width: 64 bits
       clock: 33MHz
       capabilities: vga_controller bus_master cap_list rom fb
       configuration: depth=32 driver=i915 latency=0 resolution=1920,1080
       resources: irq:49 memory:a1000000-a1ffffff memory:b0000000-bfffffff ioport:5000(size=64) memory:c0000-dffff
  *-display UNCLAIMED
       description: 3D controller
       product: GM108M [GeForce 940M]
       vendor: NVIDIA Corporation
       physical id: 0
       bus info: pci@0000:04:00.0
       version: a2
       width: 64 bits
       clock: 33MHz
       capabilities: bus_master cap_list
       configuration: latency=0
       resources: memory:a2000000-a2ffffff memory:c0000000-cfffffff memory:d0000000-d1ffffff ioport:3000(size=128) memory:a3000000-a307ffff
WARNING: output may be incomplete or inaccurate, you should run this program as super-user.
```

GeForce 940M is "UNCLAIMED".

(https://help.ubuntu.com/community/BinaryDriverHowto/Nvidia)

(https://unix.stackexchange.com/questions/341099/driver-installation-nvidia-geforce-940-mx-not-possible)

```bash
# dpkg --add-architecture i386 # Not done, maybe already done at some point
sudo apt update
sudo apt upgrade
sudo apt install bbswitch-dkms intel-microcode firmware-linux-nonfree bumblebee bumblebee-nvidia primus primus-libs primus-libs:i386 linux-headers-$(uname -r) nvidia-driver
```

If/when asked, let the system remove the previous [manual] installation of
nvidia drivers

Edit `/etc/bumblebee/bumblebee.conf`: set `KernelDriver` to `nvidia-current`.

Note: The `KernelDriver` parameter already exists with the value `nvidia`. Juste
replaced the value to `nvidia-current`.


```bash
sudo addgroup $USER bumblebee
```

In `/etc/default/grub` add the line:

```
GRUB_CMDLINE_LINUX_DEFAULT="quiet rcutree.rcu_idle_gp_delay=1"
```

Note: The `GRUB_CMDLINE_LINUX_DEFAULT` already existed with the value
`quiet`. Just added the last part to the existing parameter instead of adding a
new line.

Then update grub:

```bash
sudo update-grub
```

Now reboot.

```bash
lshw -C video
```

```
WARNING: you should run this program as super-user.
  *-display
       description: VGA compatible controller
       product: HD Graphics 5500
       vendor: Intel Corporation
       physical id: 2
       bus info: pci@0000:00:02.0
       logical name: /dev/fb0
       version: 09
       width: 64 bits
       clock: 33MHz
       capabilities: vga_controller bus_master cap_list rom fb
       configuration: depth=32 driver=i915 latency=0 resolution=1920,1080
       resources: irq:49 memory:a1000000-a1ffffff memory:b0000000-bfffffff ioport:5000(size=64) memory:c0000-dffff
  *-display
       description: 3D controller
       product: GM108M [GeForce 940M]
       vendor: NVIDIA Corporation
       physical id: 0
       bus info: pci@0000:04:00.0
       version: a2
       width: 64 bits
       clock: 33MHz
       capabilities: bus_master cap_list rom
       configuration: driver=nvidia latency=0
       resources: irq:53 memory:a2000000-a2ffffff memory:c0000000-cfffffff memory:d0000000-d1ffffff ioport:3000(size=128) memory:a3000000-a307ffff
WARNING: output may be incomplete or inaccurate, you should run this program as super-user.
```

The GeForce 940M is no more UNCLAMED \o/.

```bash
nvidia-smi
```

```
Sun Dec 19 17:38:40 2021
+-----------------------------------------------------------------------------+
| NVIDIA-SMI 470.86       Driver Version: 470.86       CUDA Version: 11.4     |
|-------------------------------+----------------------+----------------------+
| GPU  Name        Persistence-M| Bus-Id        Disp.A | Volatile Uncorr. ECC |
| Fan  Temp  Perf  Pwr:Usage/Cap|         Memory-Usage | GPU-Util  Compute M. |
|                               |                      |               MIG M. |
|===============================+======================+======================|
|   0  NVIDIA GeForce ...  On   | 00000000:04:00.0 Off |                  N/A |
| N/A   40C    P8    N/A /  N/A |      0MiB /  2004MiB |      0%      Default |
|                               |                      |                  N/A |
+-------------------------------+----------------------+----------------------+

+-----------------------------------------------------------------------------+
| Processes:                                                                  |
|  GPU   GI   CI        PID   Type   Process name                  GPU Memory |
|        ID   ID                                                   Usage      |
|=============================================================================|
|  No running processes found                                                 |
+-----------------------------------------------------------------------------+
```

There are no processes. The Nvidia card doen't seem to be used.

```bash
nvidia-settings
```

```
ERROR: Unable to load info from any available system
```

Same behaviour with sudo.

```bash
ls /etc/X11/xorg.conf.d/
```

No file for nvidia.

Add file `/etc/X11/xorg.conf.d/10-nvidia.conf` with the content:

```
Section "OutputClass"
    Identifier "nvidia"
    MatchDriver "nvidia-drm"
    Driver "nvidia"
    Option "AllowEmptyInitialConfiguration"
    ModulePath "/usr/lib/x86_64-linux-gnu/nvidia/xorg"
EndSection
```

Then reboot

Not better

Edit `/etc/default/grub` again and add for property `GRUB_CMDLINE_LINUX_DEFAULT`
value `nouveau.modeset=0 rd.driver.blacklist=nouveau`. This now gives:

```
GRUB_CMDLINE_LINUX_DEFAULT="quiet rcutree.rcu_idle_gp_delay=1 nouveau.modeset=0 rd.driver.blacklist=nouveau"
```

Then:

```bash
sudo update-grub
```

Then reboot.

`nvidia-smi` still doesn't report any processes.

```bash
lspci | egrep "3D|VGA"
```

```
00:02.0 VGA compatible controller: Intel Corporation HD Graphics 5500 (rev 09)
04:00.0 3D controller: NVIDIA Corporation GM108M [GeForce 940M] (rev a2)
```

```bash
sudo apt install nvidia-detect
nvidia-detect
```

```
Detected NVIDIA GPUs:
04:00.0 3D controller [0302]: NVIDIA Corporation GM108M [GeForce 940M] [10de:1347] (rev a2)

Checking card:  NVIDIA Corporation GM108M [GeForce 940M] (rev a2)
Your card is supported by all driver versions.
Your card is also supported by the Tesla 470 drivers series.
Your card is also supported by the Tesla 460 drivers series.
Your card is also supported by the Tesla 450 drivers series.
Your card is also supported by the Tesla 418 drivers series.
It is recommended to install the
    nvidia-driver
package.
```

Check if the graphic card is used:

```bash
glxinfo | egrep "OpenGL vendor|OpenGL renderer"
```

It should display something like:

```
OpenGL vendor string: NVIDIA Corporation
OpenGL renderer string: GeForce GT 720M/PCIe/SSE2
```

It displays this:

```
OpenGL vendor string: Intel
OpenGL renderer string: Mesa Intel(R) HD Graphics 5500 (BDW GT2)
```

The nvidia card is not used!

From here:
(https://lecrabeinfo.net/installer-pilote-proprietaire-nvidia-sur-debian-linux.html)

Try the 'Utiliser la carte graphique NVIDIA à la demande' option:

```bash
sudo apt install bumblebee-nvidia primus
sudo adduser $USER bumblebee

# Check if we can start a program with the nvidia card:
optirun glxinfo | egrep "OpenGL vendor|OpenGL renderer"
```

This prints:

```
OpenGL vendor string: NVIDIA Corporation
OpenGL renderer string: NVIDIA GeForce 940M/PCIe/SSE2
```

It works. We can use NVIDIA card for a program if we use optirun to start it.

## Try to enable NVIDIA card for all programs

Find the card's bus ID:

```bash
lspci | grep 3D
```

```
04:00.0 3D controller: NVIDIA Corporation GM108M [GeForce 940M] (rev a2)
```

The bus ID is '04:00.0'

Create or edit file `/etc/X11/xorg.conf.d/20-nvidia.conf` (replace <bus-id>):

```
Section "Module"
    Load "modesetting"
EndSection

Section "Device"
    Identifier "nvidia"
    Driver "nvidia"
    BusID "PCI:<bus-id>"
    Option "AllowEmptyInitialConfiguration"
EndSection
```

Restart X, then:

```bash
xrandr --setprovideroutputsource modesetting NVIDIA-0
xrandr --auto
xrandr --dpi 96
```

X doesn't restart. From the log file (.local/share/xorg/Xorg.0.log):

```
Failed to load module "nvidia" (module does not exist, 0)
```

```bash
ls /usr/lib/xorg/modules/drivers
```

There is `intel_drv.so` but no `nvidia*.so`.

```bash
apt-file search nvidia_drv.so
```

```
xserver-xorg-video-nvidia: /usr/lib/nvidia/current/nvidia_drv.so
xserver-xorg-video-nvidia-legacy-390xx: /usr/lib/nvidia/legacy-390xx/nvidia_drv.so
xserver-xorg-video-nvidia-tesla-418: /usr/lib/nvidia/tesla-418/nvidia_drv.so
xserver-xorg-video-nvidia-tesla-450: /usr/lib/nvidia/tesla-450/nvidia_drv.so
xserver-xorg-video-nvidia-tesla-460: /usr/lib/nvidia/tesla-460/nvidia_drv.so
xserver-xorg-video-nvidia-tesla-470: /usr/lib/nvidia/tesla-470/nvidia_drv.so
```

```bash
ls /usr/lib/nvidia/current
```

The nvidia driver is intalled but maybe not somewhere it can be found.

```bash
sudo ln -s /usr/lib/nvidia/current/nvidia_drv.so /usr/lib/xorg/modules/drivers/
```

The restart X

From X logfile:

```
[229649.234] (II) LoadModule: "nvidia"
[229649.234] (II) Loading /usr/lib/xorg/modules/drivers/nvidia_drv.so
[229649.235] (II) Module nvidia: vendor="NVIDIA Corporation"
[229649.235]    compiled for 1.6.99.901, module version = 1.0.0
[229649.235]    Module class: X.Org Video Driver
[229649.235] (II) modesetting: Driver for Modesetting Kernel Drivers: kms
[229649.235] (II) NVIDIA dlloader X Driver  470.86  Tue Oct 26 21:53:29 UTC 2021
[229649.235] (II) NVIDIA Unified Driver for all Supported NVIDIA GPUs
[229649.235] xf86EnableIOPorts: failed to set IOPL for I/O (Operation not permitted)
[229649.235] (WW) Falling back to old probe method for modesetting
[229649.235] (WW) Falling back to old probe method for modesetting
[229649.235] (II) modeset(G0): using drv /dev/dri/card0
[229649.235] (EE) No devices detected.
[229649.235] (EE)
Fatal server error:
[229649.235] (EE) no screens found(EE)
```
