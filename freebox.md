# Partage de fichiers avec le serveur Freebox

[https://doc.ubuntu-fr.org/freebox#monter_la_freebox_dans_le_systeme_de_fichier]

```bash
sudo apt install cifs-utils
sudo mount -t cifs //mafreebox.freebox.fr/Disque\ dur /home/hiesoo/freebox -o guest,iocharset=utf8,file_mode=0777,dir_mode=0777,vers=1.0
```

# Télécommande sur le PC

[https://doc.ubuntu-fr.org/freeboxv6]

