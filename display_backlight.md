# Change laptop backlight

## First try

```bash
sudo apt-get install xbacklight
xbacklight -set 50
```

> No effects

## Second try

If xbacklight doesn't work, set manually a value between 0 and 24 :

```bash
sudo sh -c "echo 5 > /sys/class/backlight/acpi_video0/brightness"
```

> Workes with Ubuntu 14.04

## Third way

Set manually a value between 0 and ... :

```bash
cat /sys/class/backlight/intel_backlight/max_brightness # Get max value
sudo sh -c "echo $value > /sys/class/backlight/intel_backlight/brightness"
```

> Works with Debian Stretch

## Fourth way

Use xrandr:

```bash
xrandr # Get output names
xrandr --output eDP-1 --brightness 1
xrandr --output eDP-1 --brightness 0.5
xrandr --output eDP-1 --brightness 0.75
```

## Fifth way

Use brightnessctl:

```bash
sudo brightnessctl set 100%
sudo brightnessctl set 50%
sudo brightnessctl set 20%-
sudo brightnessctl set +20%
```
