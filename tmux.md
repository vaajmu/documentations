# Tmux

From [this article](https://www.hamvocke.com/blog/a-quick-and-easy-guide-to-tmux/)

## Installation

```bash
sudo apt-get install tmux
```

## Usage

```bash
tmux
```

* Split horizontally with `C-b %`
* Split vertically with `C-b "`
* Switch focus with `C-b <arrow key>`
* Create a new window `C-b c`
* Switch to the previous window `C-b p`
* Switch to the next window `C-b n`
* Switch to the nth window `C-b <n>`
* Rename session `C-b $`
* Rename window `C-b ,`
* Extract a buffer to a new window `C-b !`
* Enable/disable 'monocle' `C-b z`
* Look up `C-b pageup`, then use arrows, pageup and pagedown
* Detach from session `C-b d`

Start tmux with a new named session

```bash
tmux new-session -s <session name>
```

Attach to a previous session

```bash
tmux attach-session -t <session name or index if noname>
```

List sessions

```bash
tmux list-session
```

* Move a pane to another window: `C-b ! C-b : join-pane -t <window id>`
