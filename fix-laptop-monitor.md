The laptop monitor is disabled. No idea why.

The first time, the power supply was disconnect and the screen was closed. Maybe
the battery got empty. Then, the laptop was powered and started but the screen
wasn't *on*

After som `apt-get install`s with a second screen connected with `VGA`, a
week passed with this screen connected, the laptop monitor still off and the
laptop not really used, the sceen turned on after a reboot. Again, no idea why.

One day, the screen turned off again. There is no informations about the laptop
usage. I connected with ssh and poweroff or reboot the laptop and the screen
turned back on again.

Now it is off !

# Try again to turn the screen on

Poweroff and reboot from ssh doesn't work.

I logged in to the third tty with no screen

Poweroff and start from a tty doesn't work.

```bash
echo $DISPLAY > /tmp/my-display.txt
```

From ssh :

```bash
cat /tmp/my-display.txt
```

The file is empty

[found from
google](https://superuser.com/questions/31726/how-to-disable-the-screen-linux-without-x)

This link is nice. It shows how to use xset and setterm to control screen. Also,
it shows how to control standby and suspend mode

```bash
xset dpms force off
```

```
xset:  unable to open display ""
```

```bash
setterm -powersave on
```

```
setterm: impossible de (dé)configurer le mode powersave: Ioctl() inapproprié pour un périphérique
```

(https://superuser.com/questions/617018/linux-setterm-powersave-error-cannont-unset-powersave-mode)

```bash
find /sys/ -iname blank
```

```
find: ‘/sys/kernel/debug’: Permission non accordée
/sys/devices/pci0000:00/0000:00:02.0/graphics/fb0/blank
```

```bash
sudo su
echo "1" > /sys/devices/pci0000:00/0000:00:02.0/graphics/fb0/blank
```

```bash
sudo apt-get update
sudo apt-get -y upgrade
```

(https://wiki.archlinux.org/index.php/backlight#Switching_off_the_backlight)
mentions `vbetool dpms on`


