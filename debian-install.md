# Tips on Debian Installation

## Get cryptsetup while in the installer

From [https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=641264]

When installing Debian, there are TTY available.

To use existing encrypted partitions, we need to decrypt them.

From the TTY, cryptsetup is not available.

```bash
anna-install crypto-modules cryptsetup-udeb
depmod -a
modprobe dm-mod
modprobe aes
```

Keep
[https://askubuntu.com/questions/853078/reinstall-to-existing-encrypted-partitions](this)
in mind when the installation with existing encrypted partitions is done.




