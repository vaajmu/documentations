
# If sound doesn't work

```bash
sudo apt-get install pavucontrol pulseaudio-utils pulseaudio alsa-utils alsamixergui
```

In chroot, /run must be shared along with /proc, /sys and /dev

> It works !


```bash
pacmd list-cards
pactl set-sink-port analog-output-speaker
pactl set-sink-port alsa_output.pci-0000_00_1b.0.analog-stereo/#0 analog-output-speaker
pactl set-sink-port 0 analog-output-speaker
pactl set-sink-port 0 analog-output-headphones
pactl list short sinks
pactl list short ports
pactl list short modules
pactl list short clients
pactl list short cards
pactl set-sink-port 0 analog-output-speaker
pactl set-sink-volume 0 50%
pactl set-sink-port 0 analog-output-speaker
pactl set-sink-mute 0 0
pactl set-sink-mute 0 1
pactl set-sink-mute 0 0
amixer -c 0 sset "Auto-Mute Mode" Disabled # Fails
amixer -c 1 sset "Auto-Mute Mode" Disabled

man pactl
man pacmd
man pulse-cli-syntax
man amixer
```

