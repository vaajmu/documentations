# Yunohost admin tips

## Moving app folder on another drive

App directory should be in `/var/www/<app>`.

```
sudo cp -rp /var/www/<app> /media/external/
sudo mv /var/www/<app> /var/www/bak.<app> 
sudo ln -s /media/external/<app> /var/www/<app>
sudo reboot
```

Same thing using a function:

```
#!/usr/bin/bash

set -e

OLD="$1"
NEW="$2"
USAGE="$0 <current directory location> <new location including directory name>"
EXAMPLE="$0 /var/www/lufi /drive/lufi"

[[ -z "${OLD}" ]] && echo -e "USAGE: ${USAGE}\nEXAMPLE: ${EXAMPLE}" && exit 1
[[ -z "${NEW}" ]] && echo -e "USAGE: ${USAGE}\nEXAMPLE: ${EXAMPLE}" && exit 1
! [[ -d "${OLD}" ]] && echo "Current app directory '${OLD}' does not exists" && exit 1
[[ -e "${NEW}" ]] && echo "New location '${NEW}' already exists" && exit 1

OLD_PATH=$(dirname ${OLD})
echo "Old location: '${OLD_PATH}'"
OLD_DIR=$(basename ${OLD})
echo "Old dir name: '${OLD_DIR}'"
NEW_PATH=$(dirname ${NEW})
echo "New location: '${NEW_PATH}'"
BACK_DIR="${OLD_PATH}/bak.${OLD_DIR}"

! [[ -d "${NEW_PATH}" ]] && echo "New app directory '${NEW_PATH}' does not exists" && exit 1
[[ -e "${BACK_DIR}" ]] && echo "Backup directory '${BACK_DIR}' already exists" && exit 1

echo "Copying '${OLD}' to '${NEW}'"
cp -rp "${OLD}" "${NEW}"

echo "Moving '${OLD}' to '${BACK_DIR}'"
mv "${OLD}" "${BACK_DIR}"

echo "Linking '${OLD}' to '${NEW}'"
ln -s "${NEW}" "${OLD}"

