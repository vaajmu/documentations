# Configure FTP server

From [https://www.ryadel.com/en/secure-vsftpd-ftp-server-ssl-tls-centos-7-ftps/]

## Install

```bash
sudo apt install vsftpd
```

## Edit configuration file

```bash
sudo emacs /etc/vsftpd.conf
```

## Create password for users

Passwords have max 8 characters long?

```bash
sudo mkdir /etc/vsftpd
sudo htpasswd -c /etc/vsftpd/passwd user1 # then type pwd
sudo htpasswd /etc/vsftpd/passwd user2 # then type pwd
```

## Edit configuration file

```bash
sudo emacs /etc/vsftpd.conf
```

```
listen=YES
listen_ipv6=NO
anonymous_enable=NO
local_enable=YES
virtual_use_local_privs=YES
write_enable=YES
connect_from_port_20=YES
secure_chroot_dir=/var/run/vsftpd
pam_service_name=vsftpd
guest_enable=YES
user_sub_token=$USER
local_root=/var/www/sites/$USER
chroot_local_user=YES
hide_ids=YES
```

Note that `listen` and `listen_ipv6` are mutually excusives

## Configure PAM

Edit file `/etc/pam.d/vsftpd` and replace it's content with:

```
# Customized login using htpasswd file
auth    required pam_pwdfile.so pwdfile /etc/vsftpd/passwd
account required pam_permit.so
```

## Make sure user directory exists

```bash
sudo mkdir /var/www/sites/username
```

## Restart server

```bash
sudo systemctl restart vsftpd.service
```
