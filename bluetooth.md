# Connect bluetooth keyboard without GUI

```bash
sudo apt-get install bluez-tools
bt-device -l # Don't list anything
hcitool scan # Don't list anything
```

Connect keyboard, press `Fn+C` (Bluetooth key) and try again

```bash
bt-device -l # Don't list anything
hcitool scan # List keyboard !
bt-device -c XX:XX:XX:XX:XX:XX # Error: Device not found
man hcitool
hcitool cc XX:XX:XX:XX:XX:XX # Can't create connection: Operation not permitted
sudo hcitool cc XX:XX:XX:XX:XX:XX # Doesn't seem connected
bluetoothctl # type help to list commands
```

Use the following commands in any order that works

```bash
list
devices
power on
show
scan on
pair XX:XX:XX:XX:XX:XX
trust XX:XX:XX:XX:XX:XX
devices
connect XX:XX:XX:XX:XX:XX
agent on
help
```

# Another exemple

Enter bluetooth device in pairing mode, then list from laptop bluetooth devices

```bash
hcitool scan
```

Try pairing:

```bash
bluetoothctl
show # Useless
scan on
pair FC:58:FA:C1:41:3D
paired-devices # Useless
devices # Useless
connect FC:58:FA:C1:41:3D
^D
```

# Debug device not available

```bash
sudo dmesg | grep tooth
hciconfig dev # Can't get device info: No such device
hciconfig -a # Nothing printed
hcitool scan # Device is not available: No such device
sudo modprobe -r ath3k
sudo modprobe -r ath9k
sudo modprobe ath3k
sudo modprobe ath9k
hciconfig -a # List hci0
```

# Sound device not listed in sound menu

```bash
sudo pkill pulseaudio
```

# Cannot connect to device

```bash
bluetoothctl
connect FC:58:FA:C1:41:3D # Failed to connect: org.bluez.Error.NotReady
```

```bash
sudo apt install rfkill
/usr/sbin/rfkill list
```

```
0: phy0: Wireless LAN
        Soft blocked: no
        Hard blocked: no
36: hci0: Bluetooth
        Soft blocked: yes
        Hard blocked: no
```

```bash
/usr/sbin/rfkill unblock all
/usr/sbin/rfkill list
```

```
0: phy0: Wireless LAN
        Soft blocked: no
        Hard blocked: no
36: hci0: Bluetooth
        Soft blocked: no
        Hard blocked: no
```

# Cannot control device

```bash
bluetoothctl
connect FC:58:FA:C1:41:3D
```

```
Failed to connect: org.bluez.Error.Failed
```

```bash
pulseaudio -k # Failed to kill daemon: No such process
sudo pulseaudio -k # Failed to kill daemon: No such process
pulseaudio --start
bluetoothctl
connect FC:58:FA:C1:41:3D
```

Success!

# High Fidelity Playback not available

Using `pavucontrol` in the `Configuration` tab, the bluetooth audio device
should be listed and the profile can be `High Fidelity Playback (A2DP Sink)`,
`Handsfree Hand Unit (HFP)` or `Off`.

If the High Fidelity Playback profile is unavailable, reconnect device:

``bash
bluetoothctl
# connect FC:58:FA:C1:41:3D
Attempting to connect to FC:58:FA:C1:41:3D
[NEW] Endpoint /org/bluez/hci0/dev_FC_58_FA_C1_41_3D/sep1
[NEW] Transport /org/bluez/hci0/dev_FC_58_FA_C1_41_3D/sep1/fd1
Connection successful
```

Profile should now be available.
