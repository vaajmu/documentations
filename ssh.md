# Create key

```bash
ssh-keygen -t rsa -b 4096
```

# Configure SSH Agent

This allow to not type ths SSH key passphrase every time.

```bash
ssh-add -l
```

```
The agent has no identities.
```

```bash
ssh-add ~/.ssh/my-key # Then type passphrase
```

From now on and until the specific duration time ends, the key won't need to
type the passphrase again when using it.
