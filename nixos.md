# Install and configure NixOs

## On a Raspberry Pi

Download an image from NixOs website.

Use `dd` to write the image on an SD card

Plug the SD card on the RPI and power on.

There is a shell.

Plug in a keyboard. The keyboard is in qwerty :( Command `setxkbmap` is not
installed :( but remember that we need X to use it!

Add a password for the `nixos` account. Connect an Ethernet cable from the RPI
to the laptop, add an static IP address on both. Connect over SSH onto the RPI.

Create the file `/etc/nixos/configuration.nix` with the content:

```
{ config, pkgs, lib, ... }:
{
  # NixOS wants to enable GRUB by default
  boot.loader.grub.enable = false;
  # Enables the generation of /boot/extlinux/extlinux.conf
  boot.loader.generic-extlinux-compatible.enable = true;
 
  # !!! If your board is a Raspberry Pi 1, select this:
  # boot.kernelPackages = pkgs.linuxPackages_rpi;
  # On other boards, pick a different kernel, note that on most boards with good mainline support, default, latest and hardened should all work
  # Others might need a BSP kernel, which should be noted in their respective wiki entries

  imports = [ ./hardware-configuration.nix ]; 
 
  # !!! This is only for ARMv6 / ARMv7. Don't enable this on AArch64, cache.nixos.org works there.
  # nix.binaryCaches = lib.mkForce [ "https://app.cachix.org/cache/thefloweringash-armv7" ];
  # nix.binaryCachePublicKeys = [ "thefloweringash-armv7.cachix.org-1:v+5yzBD2odFKeXbmC+OPWVqx4WVoIVO6UXgnSAWFtso=" ];
    
  # nixos-generate-config should normally set up file systems correctly
  # imports = [ ./hardware-configuration.nix ];

  # !!! Adding a swap file is optional, but recommended if you use RAM-intensive applications that might OOM otherwise. 
  # Size is in MiB, set to whatever you want (though note a larger value will use more disk space).
  swapDevices = [ { device = "/swapfile"; size = 1024; } ];
}
```

Edit things as wanted (change user name, disable lightdm, ...)

The file `hardware-configuration.nix` must be generated:

```bash
sudo nixos-generate-config
```

Don't go too far with the configuration. After minor tweaks build and try:

```bash
sudo nixos-install --root /
reboot
```

At the end of the reconfiguration, `nixos-install` will ask root password.

Add some settings in the `configuration.nix` file:

```
  networking.hostName = "nixos";
  i18n.defaultLocale = "fr_FR.UTF-8";
  time.timeZone = "Europe/Paris";
  console = {
    font = "Lat2-Terminus16";
    keyMap = "fr-bepo";
  };

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.sc = {
    isNormalUser = true;
    extraGroups = [ "wheel" "systemd-journal" ];
  };

  # Default installed packages
  environment.systemPackages = with pkgs; [
    git
    htop
    emacs
  ];

  # Enable the OpenSSH daemon.
  services.openssh.enable = true;
```

Some parameters are explained here: https://gist.github.com/santicalcagno/1860b709f0e91c861ba8f59fcad5613c

All parameters are listed here: https://search.nixos.org/options

Packages can be found here: https://search.nixos.org/packages

## Install omxplayer on Nixos & Raspberry Pi

```bash
git clone https://github.com/popcornmix/omxplayer.git
```

Don't run script `prepare-native-raspbian.sh`, it is only using dpkg. Try to run
make and install whatever is missing.

```bash
make ffmpeg
```

## Packages

Update:

```bash
sudo nix-channel --update
sudo nixos-rebuild switch
```

Install:

```bash
# Search package
nix search geany
nix search '^geany$'
nix search nixpkgs '^geany$'

# Install package
nix-env -iA nixos.geany

# List locally installed packages
nix-env -q

# List system installed packages
nixos-option environment.systemPackages

# Uninstall package
nix-env -e geany
```

Set current generation as default:

```bash
/run/current-system/bin/switch-to-configuration boot
```

## Cleaning

```bash
nix-store --gc

# List user generations
nix-env --list-generations

# List system generations
sudo nix-env --list-generations --profile /nix/var/nix/profiles/system

# Remove system generation(s)
sudo nix-env --profile /nix/var/nix/profiles/system --delete-generations 1 [2 3 ...]

# Optimize /nix using hard links when possible
nix-store --optimise -vv
```

## Nix Shell

From [https://nixos.org/guides/ad-hoc-developer-environments.html]

Purposes of nix-shell:

* use a tool that you do not have installed
* try a tool for a few minutes
* give someone else a one-liner to install a set of tools
* provide a script that is reproducible

Example:

```bash
nix-shell -p hello
```

```
error: file 'nixpkgs' was not found in the Nix search path
```

```bash
nix-shell -p nix-info --run "nix-info -m" # Same error
sudo apt purge nix-bin
sh <(curl -L https://nixos.org/nix/install) --no-daemon
. /home/hiesoo/.nix-profile/etc/profile.d/nix.sh
nix-shell -p hello
```

This will reinstall nix in the single user mode and start a shell with the
'hello' package.

`which hello` returns
`/nix/store/cv79b81pjmva78whwwpr66l5mfj4pijh-hello-2.12/bin/hello`

Use the `--pure` option to make sure that no binaries from the host can be used
inside. This makes sure that this is reproducible.

Use the `-I` to set the exact revision of the package you want.

Packages and versions can be set in a scripte: (See Reproducible executables in
the previous link).

## Fix error 'No such file or directory'

From https://nixos.wiki/wiki/Packaging/Binaries

```bash
nix-shell -p binutils stdenv wget dpkg nix-index stdenv.cc
# stdenv.cc is required for setting $NIX_CC env

# Print current interpreter:
patchelf --print-interpreter my_bin

# Patch binary with the right interpreter:
patchelf --set-interpreter "$(cat $NIX_CC/nix-support/dynamic-linker)" my_bin

# Print current interpreter:
patchelf --print-interpreter my_bin # Updated!
```

Now, we can execute the binary. It will fail if it has library dependencies but
the 'No such file or directory' error is no more.

Example of error displayed: `Accessing a corrupted shared library`

```bash
patchelf --print-needed my_bin # List all dynamic libraries that binary depends on

ldd my_bin # not a dynamic executable
```

## Find which package installs some file

```bash
# Generate index
nix-index
nix-index --help
nix-locate --help
nix-locate <pattern>
```

Not sure what to do with this

## Run ldd

From https://superuser.com/questions/912389/how-do-i-install-the-32-bit-dynamic-linker-on-64-bit-nixos

```
nix-env -i nixos.dpkg
# On Intel:
curl -O http://ftp.de.debian.org/debian/pool/main/g/glibc/libc6-i386_2.31-13+deb11u3_amd64.deb
dpkg -x libc6-i386_2.31-13+deb11u3_amd64.deb libc6-i386

# On ARM:
curl -O http://ftp.de.debian.org/debian/pool/main/g/glibc/libc6_2.31-13+deb11u3_arm64.deb
dpkg -x libc6_2.31-13+deb11u3_arm64.deb libc6-i386
```

## Install from a live system to an available partition

From https://nixos.wiki/wiki/Installing_from_Linux

Unpack NixOs rootfs into an available partition.

```bash
sudo fdisk /dev/sdb
> p # There are 18G of free space at the end of the disk
> n # Create a new partition
>   # Use default value for partition number
>   # Use default value for first sector
>   # Use default value for last sector
> w # Save new partition to disk

# The new partition is /dev/sdb4
sudo mkfs.ext4 /dev/sdb4

# Create workspace to download files
cd ~/workspace
mkdir install-nixos
cd install-nixos
mkdir -p host/nix
wget https://channels.nixos.org/nixos-22.05/latest-nixos-minimal-x86_64-linux.iso

# Extract ISO content
sudo mkdir /mnt/nixos-iso
sudo mount -o loop ./latest-nixos-minimal-x86_64-linux.iso /mnt/nixos-iso
unsquashfs -d host/nix/store /mnt/nixos-iso/nix-store.squashfs '*'

# TODO move host/ into the new partition

# TODO it seems we don't need another partition and we won't need to boot into
  it. Check why we are doing this! We are doing this because we need to shrink
  the root partition and we need a live system without our root partition
  mounted.
```

## ??

https://gist.github.com/martijnvermaat/76f2e24d0239470dd71050358b4d5134

## Install NixOs on a classic laptop

https://nixos.org/manual/nixos/stable/index.html

### Prepare installation media

Download minimal image and flash it to USB drive.

The USB drive will have two partitions, the first one is big and the second
one is not (3Mo). Mount the second partition and create a `configuration.nix`
file in it with the content:

```
{ config, pkgs, ... }: {
  imports = [
    # Include the results of the hardware scan.
    ./hardware-configuration.nix
  ];

  # boot.loader.grub.device = "/dev/sda";   # (for BIOS systems only)
  boot.loader.systemd-boot.enable = true; # (for UEFI systems only)

  # Note: setting fileSystems is generally not
  # necessary, since nixos-generate-config figures them out
  # automatically in hardware-configuration.nix.
  #fileSystems."/".device = "/dev/disk/by-label/nixos";

  # Enable the OpenSSH server.
  services.sshd.enable = true;

  users.users.<username> = {
    isNormalUser = true;
    home = "/home/<username>";
    extraGroups = [ "wheel" "networkmanager" ];
    openssh.authorizedKeys.keys = [ "<public SSH key>" ];
  };

  networking.wireless.networks = {
    echelon = {
      pskRaw = "<cyphered key>";
    };
  }
}

```

The <public SSH key> will allow a remote computer to connect to the laptop
using the <username> linux account and without entering a password.

The cyphered key can be obtain with the command:

```bash
read -s pw
# Enter plain text Wifi password
wpa_passphrase "<SSID>" "$pw"
```

### Boot installation media

Plug the USB drive to the laptop and boot.

If the flash drive is not listed in the bootable media, enter the BIOS
configuration and enable or disable legacy support.

### First steps with the installation shell

```
# Set keyboard layout:
sudo loadkeys fr

# Open NixOs manual:
nixos-help

# Install emacs
nix-env -f '<nixpkgs>' -iA emacs
```

### Partitioning

Create one boot partition and one LVM partition.

If using UEFI:

```
sudo fdisk /dev/sdX
g # Create new empty GPT partition for UEFI
n # Add 500MB partition with type ef00 EFI, this is /boot
n # Add partition with the remaining space with type 8e00 Linux LVM
w # write partition and exit
```

If using BIOS:

```
sudo fdisk /dev/sdX
o # Create new empty MSDOS partition for BIOS
n # Add primary partition with all space
a # Set bootable flag on
t # Set partition type to 8e00 Linux LVM
w # write partition and exit
```

Create encrypted partitions and mount them.

Note that if the computer uses legacy BIOS, the boot partition should be
ignored.

```
DEVICE_BOOT=/dev/sda1
DEVICE_ENC=/dev/sda2
VOLUME_NAME=enc-pc
VOLUME_GROUP_NAME=vg
PART_SWAP_SIZE=8G
PART_ROOT_SIZE=5G
PART_NIX_SIZE=50G
PART_HOME_EXTENDS="100%FREE"

sudo cryptsetup luksFormat "${DEVICE_ENC}"
sudo cryptsetup luksOpen "${DEVICE_ENC}" "${VOLUME_NAME}"

sudo pvcreate /dev/mapper/"${VOLUME_NAME}"
sudo vgcreate "${VOLUME_GROUP_NAME}" /dev/mapper/"${VOLUME_NAME}"
sudo lvcreate -L "${PART_SWAP_SIZE}" -n swap "${VOLUME_GROUP_NAME}"
sudo lvcreate -L "${PART_ROOT_SIZE}" -n root "${VOLUME_GROUP_NAME}"
sudo lvcreate -L "${PART_NIX_SIZE}" -n nix "${VOLUME_GROUP_NAME}"
sudo lvcreate -l "${PART_HOME_EXTENDS}" -n home "${VOLUME_GROUP_NAME}"

sudo mkfs.fat -F 32 "${DEVICE_BOOT}"
sudo mkswap -L swap /dev/"${VOLUME_GROUP_NAME}"/swap
sudo mkfs.ext4 -L root /dev/"${VOLUME_GROUP_NAME}"/root
sudo mkfs.ext4 -L nix /dev/"${VOLUME_GROUP_NAME}"/nix
sudo mkfs.ext4 -L home /dev/"${VOLUME_GROUP_NAME}"/home

sudo mkdir /mnt/root
sudo mount /dev/"${VOLUME_GROUP_NAME}"/root /mnt/root
sudo mkdir /mnt/root/nix
sudo mount /dev/"${VOLUME_GROUP_NAME}"/nix /mnt/root/nix
sudo mkdir /mnt/root/home
sudo mount /dev/"${VOLUME_GROUP_NAME}"/home /mnt/root/home
sudo mkdir /mnt/root/boot
sudo mount "${DEVICE_BOOT}" /mnt/root/boot
sudo swapon /dev/"${VOLUME_GROUP_NAME}"/swap

sudo blkid "${DEVICE_ENC}"
```

The last command (`blkid`) gives the UUID of the encrypted partition. It will
be used later in the `configuration.nix` file.

Copy the prepared configuration.nix and start the hardware configuration:

The prepared configuration is on the second partition of the flash drive.

```
mkdir /mnt/p2
sudo mount /dev/XXX /tmp/p2
sudo cp /mnt/p2/configuration.nix /mnt/root/etc/nixos/
sudo umount /mnt/p2

# Create the hardware configuration
sudo nixos-generate-config --root /mnt/root
```

Edit file `/mnt/root/etc/nixos/configuration.nix` and add:

```
# Use the Grub2 as bootloader.
boot.loader.grub.enable = true;
boot.loader.grub.version = 2;
boot.loader.grub.device = "nodev";
boot.loader.grub.efiSupport = true;
boot.loader.efi.canTouchEfiVariables = true;
boot.cleanTmpDir = true;

# Unencrypt partition at boot
boot.initrd.luks.devices = {
  encpc = { # Maybe 'root' should be used instead of 'encpc'
    # The UUID is known with the `blkid <device>`
    device = "/dev/disk/by-uuid/f4aaaf6d-4eb3-4a2d-a35e-f8e780ac0110";
    preLVM = true;
    allowDiscards = true;
  };
};
```

Start the installation:

```
sudo nixos-install --root /mnt/root
# Enter password for rot account if asked
sudo reboot
```

Note about Intel Graphics: https://nixos.wiki/wiki/Intel_Graphics

See below `Test GPU`

## Change configuration

Change the /etc/nixos/configuration.nix to change the machine configuration.

Then:

```
# Build the new configuration. This can be used to check that the
# configuration is valid
sudo nixos-rebuild build

# Build the configuration, set it as default for next boot and apply it to the
# running system (it will not reload the systemd services but it will run
# 'daemon-reload').
sudo nixos-rebuild switch

# Build the configuration and apply it to the running system but don't set it
# as default for the nexts boots.
sudo nixos-rebuild test

# Build the configuraiton and set it the default configuration for nexts boots
# but don't apply it to the running system.
sudo nixos-rebuild boot

# The '-p' option will create a submenu in grub.
sudo nixos-rebuild switch -p test
```

## Configuration file for multiple computers

https://github.com/cmacrae/config

The configuration can be stored away from /etc/nixos, this allow them to be
tracked by git. Then when building the system:

```
nixos-rebuild with -I nixos-config=path/to/your/configuration.nix
```

## Find packages

https://search.nixos.org/packages

## Lockscreen

Lock screen when lid is closed:

https://wiki.archlinux.org/title/Session_lock

## Device specific steps

Lenovo Thinkbook

https://wiki.archlinux.org/title/Laptop/Lenovo

## Packages

```
# Install:
nix-env -f '<nixpkgs>' -iA emacs

# Uninstall:
nix-env --uninstall <package-name>
```

## Terminal configuration

Default terminal is `foot`.


```
man foot.ini
find /usr -name "foot" -type d
# Get all default values
less /nix/store/1i2780v6frgybjrrs1lbcwsqg8jf3yyq-foot-1.13.1/etc/xdg/foot
mkdir -p ~/.config/foot
emacs ~/.config/foot # Set configuration
```

The foot.ini in /etc or in /usr lists also shortcuts:

```
# font-increase=Control+plus Control+equal Control+KP_Add
# font-decrease=Control+minus Control+KP_Subtract
# font-reset=Control+0 Control+KP_0
```

Check the whole section `[key-bindings]`.

## Upgrade system

Check at https://nixos.org/manual/nixos/stable/#sec-upgrading or
https://status.nixos.org/ what is the latest version.

```bash
sudo nix-channel --list # Get the current installed version
sudo nix-channel --remove nixos
sudo nix-channel --add https://nixos.org/channels/nixos-21.11 nixos
sudo nix-channel --update
sudo nixos-rebuild switch
```

To find the link to the last version (for the `nix-channel --add` command), go
to https://nixos.org/channels/ and copy the link to the last NixOs version.

The commands `update` and `switch` can be replaced by the single command
`nixos-rebuild switch --upgrade`.

Do not forget the "nixos" at the end of the `--add` command otherwise the
NIX_PATH variable won't be correct. (`ls -al
/nix/var/nix/profiles/per-user/root/channels/` should list `nixos` and not
`nixos-21.11`).

## Clean `/nix` directory

From https://nixos.wiki/wiki/Cleaning_the_nix_store

```bash
df -h
nix-store --gc
df -h
```

See also `https://nixos.wiki/wiki/Storage_optimization`

```bash
nix-store --gc --print-roots | grep "^/home"
du -sch $(nix-store -qR <list all 'result' directories>)
```

The last line with the total indicates the kind of space we may recover
cleaning old generations.

```bash
# List all generations:
sudo nix-env -p /nix/var/nix/profiles/system --list-generations
```

## Test GPU

From https://nixos.wiki/wiki/Intel_Graphics (see above)
From https://nixos.wiki/wiki/Accelerated_Video_Playback
From https://wiki.archlinux.org/title/Hardware_video_acceleration#Verification

```bash
vainfo > ~/vainfo.output # Can be used to see what can be decoded
vdpauinfo > ~/vdpauinfo.output 2>&1
nix-shell -p mpv
mpv --hwdec=auto <video file>
```

```
 (+) Video --vid=1 (*) (h264 1280x720 23.976fps)
 (+) Audio --aid=1 (*) 'Stereo' (aac 2ch 48000Hz)
     Subs  --sid=1 --slang=eng (ass)
Cannot load libcuda.so.1
Using hardware decoding (vaapi).
[ao/pipewire] Could not connect to context '(null)': Host is down
AO: [pulse] 48000Hz stereo 2ch float
VO: [gpu] 1280x720 vaapi[nv12]
```

```bash
sudo intel-gpu-top
```

Check GPU usage:

```bash
nix-shell -p intel-gpu-tools
sudo intel_gpu_top
```

## Temporarily open port on firewall

```bash
sudo iptables -L                      # List existing rules
sudo iptables -A INPUT -p tcp --dport 8080 -j ACCEPT
sudo systemctl reload firewall        # Restart the firewall service

sudo iptables -L INPUT --line-numbers # Identify the new rule ID
sudo iptables -D INPUT <RULE_NUM>     # Remove the rule
sudo systemctl reload firewall        # Restart the firewall service
```

## Cursor invisible

Sway need an environment variable:

```bash
WLR_NO_HARDWARE_CURSORS=1 ssh-agent sway
```

## No more sound

```bash
pgrep -l pulse
pulseaudio -k
pgrep -l pulse # PID did not changed

sudo pulseaudio -k
pgrep -l pulse # PID did not changed

sudo kill <PID>
pgrep -l pulse # PID did not changed

sudo kill -9 <PID>
pgrep -l pulse # PID did changed
```
