## Create database using Python

[https://docs.python.org/2/library/sqlite3.html]

```python
import sqlite3

createDbRequest = """
CREATE TABLE matable (
id INTEGER PRIMARY KEY NOT NULL,
quartier TEXT,
ville TEXT NOT NULL,
texte TEXT NOT NULL,
latitude REAL NOT NULL,
longitude REAL NOT NULL,
lue INTEGER DEFAULT 0,
disponible INTEGER DEFAULT 1)
"""

conn = sqlite3.connect('example.db')
c = conn.cursor()
c.execute(createDbRequest)
conn.commit()
conn.close()
```

## Access database with bash

```bash
sqlite3 database.db
.help -- Display help
.databases -- List databases
.tables -- List tables
.dump <table> -- Export <table> as text
INSERT INTO matable VALUES (1, "", "Paris", "hey there", 2, 33.5, 24.2, 0, 1);
SELECT * FROM matable;
```

## Access from a bash script

```bash
#!/bin/sh
sqlite3 test.db <<EOF
create table n (id INTEGER PRIMARY KEY,f TEXT,l TEXT);
insert into n (f,l) values ('john','smith');
select * from n;
EOF
```
