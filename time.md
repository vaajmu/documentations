# Configure timezone

```bash
sudo cp /usr/share/zoneinfo/Europe/Paris /etc/localtime
```

# Configure ntp

```bash
sudo apt-get install ntp # Nothing installed
systemctl status ntp.service # Already running
date # 10 minutes late
sudo systemctl restart ntp.service
date # Still 10 minutes late
date # Still 10 minutes late
date # time updated !
```



