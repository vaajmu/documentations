# Create an NFS server

## On the server

```bash
sudo apt-get install nfs-kernel-server nfs-common
sudo adduser nfs-client
sudo mkdir ~nfs-client/files
sudo chown -R nfs-client: ~nfs-client/files
sudo emacs /etc/exports # Add line '/home/nfs-client/files <client ip>(rw,sync,no_subtree_check)'
sudo service nfs-kernel-server restart
```

## On the client

```bash
sudo apt-get install nfs-common
sudo adduser nfs-client
sudo mkdir ~nfs-client/files
sudo chown -R nfs-client: ~nfs-client/files
sudo mount <server ip>:/home/nfs-client/files ~nfs-client/files
sudo umount ~nfs-client/files
```

Or using fstab file

```
<server ip>:<server root directory> <local mountpoint> nfs soft,timeo=14 0 0
```


