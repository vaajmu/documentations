# Configure Ctrl+arrow keys

In a termminal, use `sleep 10` command and `Ctrl+arrow key` or `Alt+arrow key`
to see the key code.

In the file `~/.inputrc`, add association beetwin the key codes and the commands
(replace `^[` by `\e`):

```
"\e[1;3C": forward-word
"\e[1;3D": backward-word
"\e[1;5C": forward-word
"\e[1;5D": backward-word

"\e[1;3A": history-search-backward
"\e[1;3B": history-search-forward
"\e[1;5A": history-search-backward
"\e[1;5B": history-search-forward
```
