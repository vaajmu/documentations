# chroot containers

## Prepare folders

```bash
cd /
sudo mkdir containers
cd containers
sudo mkdir dedicated-dirs
sudo mkdir mountpoints
```

## Download rootfs

```bash
sudo debootstrap buster rootfs
```

## Configure user in the container

```bash
sudo chroot rootfs
apt install sudo
adduser user
adduser user sudo
exit
```

## Create folders for read-only partitions

```bash
sudo mkdir mountpoints/video
sudo mkdir dedicated-dirs/video-opt
sudo mkdir dedicated-dirs/video-home
sudo chown -R $USER dedicated-dirs/video-home
```

## Start a chroot with read-only root partitions

```bash
sudo mount --bind $PWD/rootfs $PWD/mountpoints/video -o ro
sudo mount --bind $PWD/dedicated-dirs/video-opt $PWD/mountpoints/video/opt
sudo mount --bind $PWD/dedicated-dirs/video-home $PWD/mountpoints/video/home/user
xhost +
# sudo chroot mountpoints/video su - user
sudo systemd-nspawn -b -D $PWD/mountpoints/video/

# How to execute sudo:
sudo -S ls

# How to open a window
export DISPLAY=:0
firefox
```

# firejail containers

```bash
sudo apt install firejail
```

