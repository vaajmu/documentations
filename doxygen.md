# Install Doxygen

Install program and dependencies

```bash
sudo apt install graphviz doxygen doxygen-gui
```

Configure project using wizzard:

```bash
doxywizard
```

Run Doxygen:

```bash
doxygen DoxyFile
```
