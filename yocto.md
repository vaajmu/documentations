# Debug 'not found' errors

If cmake target `qt5_add_dbus_interface` is not found by bitbake but it is
defined in `recipe-sysroot-native/usr/lib/cmake/Qt5DBus/Qt5DBusMacros.cmake`

```bash
oe-pkgdata-util find-path /usr/lib/cmake/Qt5DBus/Qt5DBusMacros.cmake
```

> `qtbase-dev`

Then add `DEPENDS = "<package>"` with package being `qtbase-dev-native`,
`qtbase-dev` or `qtbase`

