# Use Google Drive connector for Linux

From (https://github.com/odeke-em/drive)

```bash
PATH='~/go/bin/':$PATH
cd gdrive
drive pull
drive push
drive push <file>
```

