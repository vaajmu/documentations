# Implement Python macros for LibreOffice

## References

[Followed Tutoriel](http://christopher5106.github.io/office/2015/12/06/openoffice-libreoffice-automate-your-office-tasks-with-python-macros.html)

[Make a package with scripts on Ubuntu](https://tmtlakmal.wordpress.com/2013/08/11/a-simple-python-macro-in-libreoffice-4-0/)

## Environment

* LibreOffice 5.2.7.2, locale EN_us.UTF-8
* Debian stretch
* Python 3.5.3

## Notes

LibreOffice directory is at `/usr/lib/libreoffice/`

Local configuration directory is at `/home/hiesoo/.config/libreoffice/4`

To run a macro in LibreOffice, Use menu Tools > Macros > Organize Macros > Python...

```bash
# Add file in a zip archive:
zip -rv <zip archive> <file path in disk> <file name in archive>

# Delete file in a zip archive (regexp available):
zip -d <zip archive> <first file path> <second file path> ... 
```

## Python shell

Open a calc document and a socket to communicate with this document:

```bash
soffice --calc --accept="socket,host=localhost,port=2002;urp;StarOffice.ServiceManager"
```

Start python (python3 is prefered)

```bash
python3
```

And type this script:

```
import uno
localContext = uno.getComponentContext()
resolver = localContext.ServiceManager.createInstanceWithContext("com.sun.star.bridge.UnoUrlResolver", localContext)
ctx = resolver.resolve( "uno:socket,host=localhost,port=2002;urp;StarOffice.ComponentContext" )
smgr = ctx.ServiceManager
desktop = smgr.createInstanceWithContext( "com.sun.star.frame.Desktop",ctx)
model = desktop.getCurrentComponent()
active_sheet = model.CurrentController.ActiveSheet
cell1 = active_sheet.getCellRangeByName("C4")
cell1.String = "Hello world"
cell2 = active_sheet.getCellRangeByName("E6")
cell2.Value = cell2.Value + 1
```

This will print "Hello world" on cell C4 and "1" to cell E6.

## Add macro in LibreOffice install dir

Save the following script in /usr/lib/libreoffice/share/Scripts/python/test-python.py:

```python
import sys
def PythonVersion(*args):
    """Prints the Python version into the current document"""

    #get the doc from the scripting context which is made available to all scripts
    desktop = XSCRIPTCONTEXT.getDesktop()
    model = desktop.getCurrentComponent()

    #check whether there's already an opened document. Otherwise, create a new one
    if not hasattr(model, "Text"):
        model = desktop.loadComponentFromURL("private:factory/swriter","_blank", 0, () )

    #get the XText interface
    text = model.Text

    #create an XTextRange at the end of the document
    tRange = text.End

    #and set the string
    tRange.String = "The Python version is %s.%s.%s" % sys.version_info[:3] + " and the executable path is " + sys.executable
    return None
```

Open a new Writer document and Use menu Tools > Macros > Run Macro... >
LibreOffice Macros > test-python > PythonVersion

It seems a macro can be packaged to be distributed. The feature is described
[here](https://tmtlakmal.wordpress.com/2013/08/11/a-simple-python-macro-in-libreoffice-4-0/)

## Add macro in a LibreOffice document

Use this python script to add a script in a document:

```bash
import zipfile
import shutil
import os
import sys

print("Delete and create directory with_macro")
shutil.rmtree("with_macro",True)
os.mkdir("with_macro")

filename = "with_macro/"+sys.argv[1]
print("Open file " + sys.argv[1])
shutil.copyfile(sys.argv[1],filename)

macro = sys.argv[2]

doc = zipfile.ZipFile(filename,'a')
doc.write(macro, "Scripts/python/" + macro)
manifest = []
for line in doc.open('META-INF/manifest.xml'):
    if '</manifest:manifest>' in line.decode('utf-8'):
        for path in ['Scripts/','Scripts/python/','Scripts/python/' + macro]:
            manifest.append(' <manifest:file-entry manifest:media-type="application/binary" manifest:full-path="%s"/>' % path)
    manifest.append(line.decode('utf-8'))

doc.writestr('META-INF/manifest.xml', ''.join(manifest))
doc.close()
print("File created: "+filename)
```

```bash
python3 include-macro.py <macro filename> <document filename>
```

The script doesn't really works. Use the script include-macro.sh:

```bash
./include-macro.sh writer.odt test-python.py generated-writer.odt
unzip -l generated-writer.odt # Check Script/python/test-python.py is listed
unzip -p generated-writer.odt META-INF/manifest.xml # Check that Script, Script/python and  are present
```

Now open the file and run the macro "in the document" instead of the one in
"Libre Office". Change macro security option if needed in Tools > Option >
LibreOffice > Security > Macro Security > Macro Security... select Medium. Low
will work but is not recommended.

To avoid the warning message when opening the document, the script can be saved
in `$HOME/.config/libreoffice/4/user/Scripts/python/`. The document must be
re-opened when the script changes on hard drive. A link can be used also.

## Select a file with a FilePicker

```python
import sys
import csv
import uno
from com.sun.star.ui.dialogs.TemplateDescription import FILEOPEN_SIMPLE
from pprint import pprint

def SelectFile(*args):
    """Select a file in hard drive"""

    context = uno.getComponentContext()
    serviceManager = context.getServiceManager()

    filePicker = serviceManager.createInstanceWithArgumentsAndContext(
        'com.sun.star.ui.dialogs.FilePicker',
        (FILEOPEN_SIMPLE,),
        context
    )

    filePicker.Title = 'Select file to import'

    filePicker.execute()
    # Selected files (might be more than one) is stored in a list in
    # filePicker.Files

    return filePicker.Files[0]
```

## Import data from csv file

```python
import sys
import csv

def ImportData(*args):
    """Import raw data from a csv file"""

    fileContent = ""

    with open('./comptabilite.csv', 'r') as csvfile:
        reader = csv.reader(csvfile, delimiter=',')
        for row in reader:
            # Row is an array
            print('#'.join(row))

    return None
```
