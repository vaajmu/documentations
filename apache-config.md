
# Configure Apache server

## Configure VirtualHost

To use one server for multiple websites (domain names):

### Create the website directory

```bash
sudo mkdir /var/www/<website id>
sudo mkdir /var/www/<website id>/logs
sudo mkdir /var/www/<website id>/website
```

### Enable virtual hosts

Check `/etc/apache2/conf.d/virtual.conf` doesn't exists. Create it and add:

```
#
#  We're running multiple virtual hosts.
#
NameVirtualHost *
```

### Configure website

Website configuration must be stored at `/etc/apache2/sites-available`. If it
must be enabled, a symbolic link is created at `/etc/apache2/sites-enabled`. The
website at the first directory won't be enabled at apache startup. To do this,
there is the `a2ensite` and `a2dissite` command

Edit `/etc/apache2/sites-available/<website id>` with content:

```
#
#  Example.com (/etc/apache2/sites-available/www.example.com)
#
<VirtualHost *>
        ServerAdmin webmaster@example.com
        ServerName  www.example.com
        ServerAlias example.com

        # Indexes + Directory Root.
        DirectoryIndex index.html
        DocumentRoot /home/www/www.example.com/htdocs/

        # CGI Directory
        ScriptAlias /cgi-bin/ /home/www/www.example.com/cgi-bin/
        <Location /cgi-bin>
                Options +ExecCGI
        </Location>


        # Logfiles
        ErrorLog  /home/www/www.example.com/logs/error.log
        CustomLog /home/www/www.example.com/logs/access.log combined
</VirtualHost>
```

`www.example.com` is also the website id.

Remove the CGI section as we won't use CGI

Use directory `website` instead of `htdocs`

### Enable website

```bash
a2ensite <website id> # ERROR: Site <website id> does not exist!
sudo /etc/init.d/apache2 reload
sudo systemctl daemon-reload # Only the first time ?
sudo mv /etc/apache2/sies-available/<website id> /etc/apache2/sies-available/<website id>.conf
a2ensite <website id> # Works
sudo /etc/init.d/apache2 reload
```

Try with Firefox. Website is not updated

```bash
sudo a2dissite 000-default
sudo service apache2 reload
```

> It works !

## Configure HTTPS

### Genereate certificate

`acme.sh` allow to create SSL certificates and configure them with `Let's
encrypt`.

```bash
wget -O -  https://get.acme.sh | sh # Open a new terminal to get the acme.sh command
acme.sh --issue -d <website domain name> -w <website root directory>
```

It fails. Try again. Connexction refused because limit rate is reached. Try with
the --staging flage to use the test server.

```bash
acme.sh --staging --debug --issue -d <website domain name> -w <website root directory>
```

Open port 443 (HTTPS) and try again. It fails

```bash
wget https://dl.eff.org/certbot-auto
chmod a+x certbot-auto
sudo ./certbot-auto -w /var/www/<website id>/website/ -d <domain name> -d www.<domain name> --test-cert --apache -n --agree-tos --email "<email>"
```

> It works

Do it again without the `--test-cert` flag. It works but cannot access to HTTPS
website.

```bash
sudo rm /etc/apache2/sites-available/<website id>-le-ssl.conf
sudo rm /etc/apache2/sites-enabled/<website id>-le-ssl.conf
sudo ./certbot-auto -w /var/www/<website id>/website/ -d <domain name> -d www.<domain name> --apache -n --agree-tos --email "<email>"
sudo service apache2 reload
```

Edit root crontab with command `sudo crontab -e` and add the line:

```
0 0,12 * * * python -c 'import random; import time; time.sleep(random.random() * 3600)' && /home/pi/workspace/certbot/certbot-auto renew 
```

### Force HTTPS

To avoid users using the HTTP protocol, we need to redirect all HTTP connections to HTTPS

Edit `/etc/apache2/sites-enabled/<website id>.conf` and add

```
Redirect permanent / https://<domain name>
```

The `DocumentRoot` can be removed. Then restart (reload?) apache server

### Manually renew certificates

sudo /usr/local/bin/certbot-auto renew

