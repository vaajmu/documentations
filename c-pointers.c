#include <stdio.h>

int f1() {

    int a = 1;
    const int* b = &a;
    int* const c = &a;
    int const * d = &a;

    b = &a;
    // *b = 0;
    a = 2;
    // c = &a;
    *c = 3;
    // *d = 4;
    d = &a;

    return 0;
}

int f2() {
    int a = 0;
    int* b = &a;
    int** c = &b;
    const int** d = NULL;
    int const ** e = NULL;
    int * const * f = NULL;
    int * * const g = NULL;

    a = 0;
    b = &a;
    *b = 0;
    c = &b;
    *c = b;
    **c = 0;
    d = NULL;
    *d = NULL;
    /* **d = 0; */
    e = NULL;
    *e = NULL;
    /* **e = NULL; */
    f = NULL;
    /* *f = NULL; */
    **f = 0;
    /* g = NULL; */
    *g = NULL;
    **g = 0;
}

int main() {
    f1();
    f2();
}
