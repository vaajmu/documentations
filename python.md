# Python

## Show package installed version

```
pip show flask
```

## Document and test code with doctest

https://realpython.com/python-doctest/

python-doctest is installed with python. It is a way to write documentation
with examples and expected results and run this examples and check the
results!

It is a good solution because:

- The functions (particularily APIs) will be documented.

- If the functions changes and the comments are outdated, the examples will
probably not work anymore. This forces the developper to update the comments.

- Exemples are a really good way to understand how to use the code.

- The written functions will be tested!

- The tests are easily written.

- The documentation can serve as acceptance tests, anti-regression tests and
  integration tests.

Example:

```python
# calculations.py

def add(a, b):
    """Compute and return the sum of two numbers.

    Usage examples:
    >>> add(4.0, 2.0)
    6.0
    >>> add(4, 2)
    6.0
    """
    return float(a + b)
```

Run the tests:

```bash
python -m doctest calculations.py
```

No output means that everything went well.

The verbose mode will show the tests that are run and will sum up the results.

```bash
python -m doctest -v calculations.py
```

will return:

```
Trying:
    add(4.0, 2.0)
Expecting:
    6.0
ok
Trying:
    add(4, 2)
Expecting:
    6.0
ok
1 items had no tests:
    calculations
1 items passed all tests:
   2 tests in calculations.add
2 tests in 2 items.
2 passed and 0 failed.
Test passed.
```

To test the function's output:

```python
# printed_output.py

def greet(name="World"):
    """Print a greeting to the screen.

    Usage examples:
    >>> greet("Pythonista")
    Hello, Pythonista!
    >>> greet()
    Hello, World!
    """
    print(f"Hello, {name}!")
```

Catching exceptions:

Trying the exception in the REPL:

```python
>>> divide(42, 0)
Traceback (most recent call last):
    ...
ZeroDivisionError: division by zero
```

The copying the result:

```python
# calculations.py
# ...

def divide(a, b):
    """Compute and return the quotient of two numbers.

    Usage examples:
    >>> divide(84, 2)
    42.0
    >>> divide(15, 3)
    5.0
    >>> divide(42, -2)
    -21.0

    >>> divide(42, 0)
    Traceback (most recent call last):
    ZeroDivisionError: division by zero
    """
    return float(a / b)
```

Run tests written in the README:

```bash
python -m doctest -v README.md
```

Note that the code may need to be in a code section (using triple back-quotes)
and the last line before the ending back-quotes must be empty, otherwise the
tool would consider it as part of the code.

Run tests written in an other file:

```bash
python -m doctest -v test_calculations.txt
```

It is usefull to not add too much tests in the functions documentation.

In a similar way, other places where adding examples is a good option is the
documentation of the package, the module and the class.

## Test using `unittest`

https://docs.python.org/3/library/unittest.html

### Basic example:

```python
import unittest

class TestStringMethods(unittest.TestCase):

    def test_upper(self):
        self.assertEqual('foo'.upper(), 'FOO')

    def test_isupper(self):
        self.assertTrue('FOO'.isupper())
        self.assertFalse('Foo'.isupper())

    def test_split(self):
        s = 'hello world'
        self.assertEqual(s.split(), ['hello', 'world'])
        # check that s.split fails when the separator is not a string
        with self.assertRaises(TypeError):
            s.split(2)

if __name__ == '__main__':
    unittest.main()
```

The name of the test functions should start with `test_`.

There are a lot of assert functions and they are listed here:
https://docs.python.org/3/library/unittest.html#unittest.TestCase.assertEqual

### Setup and teardown steps

```python
import unittest

class WidgetTestCase(unittest.TestCase):
    def setUp(self):
        self.widget = Widget('The widget')

    def tearDown(self):
        self.widget.dispose()
```

### Running tests

Running the test or parts of the test:

```bash
python -m unittest test_module1 test_module2
python -m unittest test_module.TestClass
python -m unittest test_module.TestClass.test_method
python -m unittest tests/test_something.py
```

Note: there is a verbose option `-v`.

