# Install games SuperTux

## Configuration

Debian testing (stretch)

## Get SuperTux

Download/extract package from Internet

## Compile SuperTux

```bash
cd <package sources>
mkdir build
sudo apt-get install cmake 
cmake ../
```

Error from cmake: `Could NOT find OpenAL (missing: OPENAL_LIBRARY OPENAL_INCLUDE_DIR)`

```bash
sudo apt-get install libopenal1 # 0 upgraded, 0 newly installed, 0 to remove and 256 not upgraded
sudo apt-get install libopenal1-dbg # 0 upgraded, 1 newly installed, 0 to remove and 256 not upgraded
```

Useless

Read INSTALL.md, requirements part

```bash
sudo apt-get install cmake libsdl2-dev libsdl2-2.0-0 libsdl-image1.2 libsdl-image1.2-dev libglew2.0 libglew-dev libboost-all-dev libcurl3 libogg0 libvorbis0a
cmake ../
```

Less errors, try to resolve openal dependency

```bash
export OPENAL_LIBRARY=/usr/lib/x86_64-linux-gnu
export OPENAL_INCLUDE_DIR=/usr/lib/x86_64-linux-gnu
cmake ../
sudo apt-get update
sudo apt-get upgrade
sudo apt-get install libopenal-dev
cmake ../
```

No more openal error message, try to resolve SDL2_image dependency

```bash
sudo apt-get install libsdl2-image-2.0-0 libsdl2-image-dev libsdl2-2.0-0 libsdl2-dev
cmake ../
```

Still not

```bash
sudo apt-get install libogg-vorbis-decoder-perl libogg-vorbis-header-pureperl-perl
cmake ../
```

Error for SDL_image resolved, try to resolve oggvorbis error

```bash
sudo apt-get install dir2ogg
cmake ../
sudo apt-get install libogg0 libogg-dev libogg-dbg
cmake ../
sudo apt-get install libvorbis-dbg libvorbis-dev libvorbis0a vorbis-tools vorbis-tools-dbg
cmake ../
```

Error for oggvorbis resolved, new error with CURL

```bash
sudo apt autoremove
```

Try to resolve `Could NOT find PhysFS (missing: PHYSFS_LIBRARY PHYSFS_INCLUDE_DIR)`

```bash
apt-cache search physfs
sudo apt-get install libphysfs-dev libphysfs1
cmake ../
```

PhysFS found, try to resolve `Could NOT find CURL (missing: CURL_LIBRARY CURL_INCLUDE_DIR)`

```bash
apt-cache search curl
sudo apt-get install curl libcurl3 libcurlpp-dev libcurlpp0
cmake ../
cd ../
rm -r build
./configure # Not better
sudo apt-get install libncurses5 libncurses5-dev libcurl4-openssl-dev
cd build
cmake ../ # Success !!!
make
./supertux2
```


