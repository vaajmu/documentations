# Install Adobe Flash Player on debian

## Environment

OS: debian stretch 9.2

Browser: Mozilla Firefox

Date: December 2017

## Prerequisites

Check firefox plugins directory, like `/usr/lib/mozilla/plugins/`. This will be
referred as `mozilla plugins directory`

## Installation

Download flash archive (.tar.gz), downloaded from their website

```bash
mkdir /tmp/flash-plugins
cd /tmp/flash-plugins
tar xzf <archive path>
sudo cp libflashplayer.so <mozilla plugins directory>
sudo cp -r /usr/* /usr
```

Now restart **all** firefox processes

