# How to use ino

Ino allowto build programs and push them on the arduino

## Links

http://inotool.org/quickstart

## Install

```bash
pip install ino
sudo apt-get install arduino picocom
```

The ino binary is placed in `~/.local/bin`, make sure it is in the PATH.

Serial communication needs to be root or to be part of the dialout group. To
prevent running the `ino serial` as root, use the commad

```bash
sudo adduser $USER dialout
```

Then logout and log back in.

## Init the project

```bash
mkdir arduino-project
cd arduino-project
ino init
```

## Add source code

The source code is placed in the src folder. The main function is in the
sketch.ino

## Build project

```bash
ino build
```

## Push project

```bash
ino upload
```

## Listen on serial

```bash
ino serial
```

Use Ctrl+a Ctrl+x To exit

## Configuration

Configuration file can be in `/etc/ino.ini`, `~/.inorc` or `./ino.ini`

Dummy example:

```
board-model = mega2560
serial-port = /dev/ttyACM1

[build]
board-model = mega2560

[upload]
board-model = mega2560
serial-port = /dev/ttyACM1

[serial]
serial-port = /dev/ttyACM1
```

The first part is the 'unamed section', parameters here will apply for each
section.
