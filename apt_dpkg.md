# Memo about `dpkg` and `apt`

## Get package sources

`apt-get source <package name>`

## Get package deb file

`apt-get download <package name>`

## Get compile flags for a packaeg

`pkg-config --cflags --libs <package name>`

ie `pkg-config --cflags --libs gio-2.0`

## List package files

### From `.deb` file

`dpkg -c <.deb file>`

### From `apt`

```bash
sudo apt-get install apt-file
sudo apt-file update
apt-file list <package_name>
```

## Choose package version

```bash
apt-cache showpkg <pkg name> # List available versions
apt-cache policy <pkg name> # Print installed versions
sudo apt-get install <package-name>=<package-version-number>
sudo apt-get -t=<target release> install <package-name>
```

## 'have been kept back'

Solve the `The following packages have been kept back` message:

```bash
sudo apt update
```

```
Reading package lists... Done
Building dependency tree... Done
Reading state information... Done
Calculating upgrade... Done
The following packages have been kept back:
    <packages list>
```

```bash
sudo apt-get --with-new-pkgs upgrade # Not enough
apt-get dist-upgrade
```

## Add repo

For exemple with openvpn:

```bash
sudo emacs /etc/apt/sources.list.d/openvpn-aptrepo.list
```

Set this content:

```
deb http://build.openvpn.net/debian/openvpn/release/2.5 buster main
```

```bash
apt update
```

```
W: GPG error: http://build.openvpn.net/debian/openvpn/release/2.5 buster
InRelease: The following signatures couldn't be verified because the public
key is not available: NO_PUBKEY 8E6DA8B4E158C569
```

```bash
apt-key list
```

See https://chrisjean.com/fix-apt-get-update-the-following-signatures-couldnt-be-verified-because-the-public-key-is-not-available/

This page describe how to blindly add a new unverified key. Not really good.

See https://itsfoss.com/solve-gpg-error-signatures-verified-ubuntu/

Go to the repo webpage using a browser:
https://build.openvpn.net/debian/openvpn/release/2.5/ and download the public
key, then install it:

```bash
sudo apt-key add /tmp/pubkey.gpg
sudo apt update
```


