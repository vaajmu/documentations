
# Debug threads

```gdb
info threads # Print threads id
thread <tid> # Debug this thread
```

# Get function context

```gdb
# List frames with
bt

# Select frame with
frame <n>
```

# Print constant value

```gdb
info macro <CONSTANT>
```

# Print enum value

??

# Don't stop program when signals are received

```
# List current behaviour
info signals
info handle
info signals SIGINT

# Change behaviour for a signal
handle SIGINT nostop print pass
```

# Stop when a variable is changed

```
watch <variable>
watch -l <variable> # watches the address
```

# Continually print expression value

This will print the variable value each time the user is prompted

```
display <variable>
```

# Stop displaying an expression

```
undisplay <display id>
```

Note: the display id is printed everytime before the expression.

# Execute commands when breakpoint is hit

The `commands` must be after adding a breakpoint (actually I don't know how to
set commands for a specific breakpoint).

```
b <file>:<line>
commands
<command1>
<command2>
end
```

There can be more commands.

If `silent` is one of the command, when the breakpoint hits there won't be the
displayed line with the breakpoint ID and the current line.

One of the commands can be `continue` to actually not stop the program longer
than it should be.

# Go to a specified line

```
advance <linespec>
```

# Display the current executed line

```
frame
```

# Print debugged process

```
info inferior
```

# Change the debugged executable

```
file <path/to/file>
```

# Show type of a variable

```bash
ptype variable
```

# Enable pretty print

This (probably among other things) display the structures with one line per
fields and deal with structures within structures instead of putting
everything in one line only.

```bash
set print pretty on
p <var>
```

# Convenience variables

https://sourceware.org/gdb/current/onlinedocs/gdb/Convenience-Vars.html

Use temporary variables declared inside GDB:

```
set $foo = *object_ptr # Declare/set value to '$foo'
show convenience       # List convenience variables
p $foo                 # Print '$foo' value
```

# Print some bytes from an array

```
# Consider an unsigned char *buf
p /x buf@4 # Prints the first 4 bytes from 'buf'
```
