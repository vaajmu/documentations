# Mount LVM partition

[More here](https://superuser.com/questions/116617/how-to-mount-an-lvm-volume)

[And here](https://askubuntu.com/questions/63594/mount-encrypted-volumes-from-command-line)

If volume is encrypted, use cryptsetup to access it:

```bash
lsblk # Get crypted device
sudo cryptsetup luksOpen /dev/sda1 <volume group name>
lsblk # Volume group is listed
```

```bash
sudo vgscan # List volume groups, may be unnecessary
sudo lvs # List LVM partitions, may be unnecessary
sudo mount /dev/<physical name>/<logical name> <mountpoint>
```

# Don't read some partition at boot

In file `/etc/crypttab`, comment out (with `#` ?) the ignored volume



