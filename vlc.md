# VLC tips

## VLC crash

```
$ vlc video.mp4 
VLC media player 3.0.21 Vetinari (revision 3.0.21-0-gdd8bfdbabe8)
[000055a2752a5670] main libvlc: Running vlc with the default interface. Use 'cvlc' to use vlc without interface.
[00007f6b40003b40] gl gl: Initialized libplacebo v2.72.0 (API v72)
libva info: VA-API version 1.10.0
libva info: Trying to open /usr/lib/x86_64-linux-gnu/dri/iHD_drv_video.so
libva info: Found init function __vaDriverInit_1_10
libva info: va_openDriver() returns 0
[00007f6b50c02170] avcodec decoder: Using Intel iHD driver for Intel(R) Gen Graphics - 21.1.1 () for hardware decoding
[h264 @ 0x7f6b510f6100] A hardware frames or device context is required for hardware accelerated decoding.
[h264 @ 0x7f6b510f6100] Failed setup for format vaapi_vld: hwaccel initialisation returned error.
[00007f6b50c02170] avcodec decoder error: existing hardware acceleration cannot be reused
[00007f6b4085f160] gl gl: Initialized libplacebo v2.72.0 (API v72)
libva info: VA-API version 1.10.0
libva info: Trying to open /usr/lib/x86_64-linux-gnu/dri/iHD_drv_video.so
libva info: Found init function __vaDriverInit_1_10
libva info: va_openDriver() returns 0
[00007f6b50c02170] avcodec decoder: Using OpenGL/VAAPI backend for VDPAU for hardware decoding
Assertion !p->parent->stash_hwaccel failed at src/libavcodec/pthread_frame.c:649
Aborted (core dumped)
```

At some point/when moving forward, VLC crashes.

```bash
vlc --avcodec-hw=none video.mp4 # works
```
