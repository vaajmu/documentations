# Wireshark

Write LUA scripts for Wireshark.

https://www.wireshark.org/docs/wsdg_html_chunked/wsluarm.html

Default personnal configuration directory:

* $XDG_CONFIG_HOME/wireshark;
* $HOME/.wireshark;

Default personal plugin folder:

* ~/.local/lib/wireshark/plugins;
* Or maybe $HOME/.wireshark/plugins??;

To know where the plugins are installed, Go to Wireshark GUI -> Help -> About
Wireshark -> Folders and check for `Personal plugins` and `Personal Lua
plugins`.

## Examples

* https://wiki.wireshark.org/Lua/Examples/PostDissector
* https://wiki.wireshark.org/Lua/Dissectors#postdissectors

## No LUA but ???
