# How to use tcpdump

## Options

```
-i <interface> // select interface
-v // verbose
-vv // more verbose
-A // show ASCII text
-w <file> // Save trafic in PCAP format into <file>
port <port> // filter with source and dest port
udp // filter UDP packets
tcp // filter TCP packets
host <IP> // filter source and dest IP
dst port <port> // filter destination port
<filter> <or/and> <filter> // and/or conditions
not <filter> // filter out the <filter> condition
```

