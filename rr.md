# Debugging with RR

## Install

```bash
sudo apt install rr
```

Actually the packaged version (5.4) is too old and have issues.

Download the version 5.5 from
https://github.com/rr-debugger/rr/releases/download/5.5.0/rr-5.5.0-Linux-x86_64.deb.

```bash
sudo apt purge rr
sudo dpkg -i rr-5.5.0-Linux-x86_64.deb
```

Then configure system. Edit file `/etc/sysctl.conf` and add
`kernel.perf_event_paranoid = 1` at the end.

Then apply the modification: `sudo sysctl -p`

## Usage

Record:

```bash
rr record <program> <args>
```

Wait for the program to crash. If no crash is expected (a,d you want to debug
what is run) you can also cut the execution with Ctrl+C. The start the replay:

```bash
rr replay
```

By default, the last record will be replayed.

To use another "frontend" instead of GDB, for example `cgdb`:

```bash
rr replay -d cgdb
```

When rr starts, the program will be loaded and rr will stop. Make it continue
with the `c` command, use the commands normally available with GDB.

## Advance backwards

Set a breakpoint where you want to stop then use `reverse-continue`:

```
b file:line
reverse-continue
```

In case of a received signal (like SIGINT), the signal will be triggered
again. You need to run `reverse-continue` again to "step over".

## Events number

From https://github.com/rr-debugger/rr/wiki/Usage

Events number are identifier of an event in RR. An event is everything that
happened in the program.

If RR is started with `-M` it will display the event numbers.

The event number of the current location can be found using the command
`where`:

```
(gdb) when
$12 = 33818
(gdb) run 32818
... restarts 1000 events earlier ...
```
