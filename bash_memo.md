# Memento Bash

## Print function body

declare -f <function name>

## Remove a file with strange name

```bash
ls -latr
```

```
-rw-r--r--  1 anloh anloh 70729 Feb  5 18:11 ?OA?OA?OB
```

```bash
ls | hexdump -C
```

```
00000150  1b 4f 41 1b 4f 41 1b 4f  42 0a 54 4f 44 4f 2e 6d  |.OA.OA.OB.TODO.m|
00000160  64 0a                                             |d.|
```

```bash
man ascii
```

```
       033   27    1B    ESC (escape)                133   91    5B    [
       017   15    0F    SI  (shift in)              117   79    4F    O
```

The first point is the escape key

```bash
mv $(printf "\x1b0A\x1b0A\x1B0B") truc
```

## Remove part of a string

Extract a file directory

```bash
filePath="/truc/coucou/file.txt"
echo ${filePath%%/*}
```

## Exclude a directiory from a recursive grep

```bash
grep -R --exclude-dir='bionic' --exclude-dir="abi" "LOCAL_PATH" abi art bionic
```

## Exclude a directory from a find search

```bash
find . -name .repo -prune -o -name .git -prune -o -name out -prune -o -type f -name "*\.java" -exec grep --color -n "$@" {} +
```

## Exclude files with extension from 'find' search

This includes *.c, *.h, ... and *.blk files and exclude *.blk.c files.

```bash
find . ! \( -name "*.blk.c" \) -a -name "*.[chxsS]" -o -name "*.blk"
```

## Prevent grep aliases to be used in make autocompletion

When an alias is set for grep, when we use double-tab with make, it will use
this alias.

To prevent this, we must ask to make to use vanilla-grep for this functionnality.

```bash
cat /etc/bash_completion
ls /usr/share/bash-completion/
ls /usr/share/bash-completion/completions/make
```

Replace `grep` in `/usr/share/bash-completion/completions/make` by `\grep`

## Colorful logs

```bash
_log() {
    NC="\033[0m"
    COLOR="$1"
    TAG="$2"
    MSG="$3"
    echo -en "${COLOR}"
    # Don't force -e option for the user message
    echo -n "${TAG} ${MSG}"
    echo -e "${NC}"
}

loge() {
    COLOR="\033[0;31m"
    TAG="[ERROR]"
    MSG="$@"
    _log "${COLOR}" "${TAG}" "${MSG}"
}

logi() {
    COLOR="\033[0;32m"
    TAG="[INFOS]"
    MSG="$@"
    _log "${COLOR}" "${TAG}" "${MSG}"
}

log1() {
    COLOR="\033[0;37m"
    TAG=" [LOG1]"
    MSG="$@"
    _log "${COLOR}" "${TAG}" "${MSG}"
}

log2() {
    COLOR="\033[0;37m"
    TAG=" [LOG2]"
    MSG="$@"
    _log "${COLOR}" "${TAG}" "${MSG}"
}
```

## Execute function at the end of script

```bash
trap "cleanup" EXIT

SCRIPT_STATUS="unknown"
TMP_FILE=$(mktemp)
OUTPUT_FILE="$1"

deleteFileIfExists() {
    FILE="$1"
    if [[ -f "${FILE}" ]]
    then
        rm "${FILE}"
    elif [[ -d "${FILE}" ]]
    then
         rmdir "${FILE}"
    fi
}

cleanup() {
    deleteFileIfExists "{TMP_FILE}"

    if ! [[ "${SCRIPT_STATUS}" = "done" ]]
    then
        loge "Done with errors"
        [[ -f "${OUTPUT_FILE}" ]] && rm "${OUTPUT_FILE}"
    else
        logi "Done with success"
    fi
}
```

## Check script

Tool shellcheck can analyse a bash script and detect some errors or lack of best
practices.

```
sudo apt-get install shellcheck
shellcheck <script file>
```

## Use dates

```bash
# Print current timestamp
date +"%s"

# Convert specified timestamp to date
date -d @1267619929

# Set a date
date -s @'1271251273'
```

## Use tar to extract `.tar.xz` files

```bash
sudo apt install xz-utils
```

Now tar can extract .tar.xz files ...
