

```bash
sudo apt install debootstrap qemu
sudo debootstrap --arch arm64 buster /mnt/sd http://ftp.uk.debian.org/debian
```

```
Unable to execute target architecture
```

```bash
sudo debootstrap --arch arm64 --foreign buster . http://ftp.fr.debian.org/debian
sudo apt-get install qemu qemu-system-arm
sudo apt install qemu-user qemu-user-static
sudo cp /usr/bin/qemu-arm-static rootfs/usr/bin
sudo chroot .
/debootstrap/debootstrap --second-stage
apt update

exit # Quit chroot

sudo mount -t proc /proc proc/
sudo mount -t sysfs /sys sys/
sudo mount -o bind /dev dev/
sudo chroot . /bin/bash

apt install sudo git emacs
useradd pi
passwd pi
mkdir /home/pi
chown -R pi: /home/pi
visudo # Add NOPASSWD for group sudo
nano /etc/passwd # change shell to bash for user pi

su - pi
git clone https://github.com/popcornmix/omxplayer.git
cd omxplayer/
./prepare-native-raspbian.sh
sudo apt-get update && sudo apt-get install  git-core binutils libasound2-dev libva1 libpcre3-dev libidn11-dev libboost-dev libfreetype6-dev libdbus-1-dev libssl1.0-dev libssh-dev libsmbclient-dev gcc g++ pkg-config

# Change libssl1.0-dev to libssl-dev
# Try without libva1

sudo apt-get update && sudo apt-get install  libraspberrypi-dev libraspberrypi0 libraspberrypi-bin

emacs /etc/apt/surces.list/raspberrypi.list # Add 'deb http://raspbian.raspberrypi.org/raspbian/ buster main contrib non-free rpi'
sudo apt update
```

```
sudo: unable to resolve host PO2: Name or service not known
Hit:1 http://ftp.fr.debian.org/debian buster InRelease
Get:2 http://raspbian.raspberrypi.org/raspbian buster InRelease [15.0 kB]
Err:2 http://raspbian.raspberrypi.org/raspbian buster InRelease                                                                       
  The following signatures couldn't be verified because the public key is not available: NO_PUBKEY 9165938D90FDDD2E
Reading package lists... Done                                                                                                         
W: GPG error: http://raspbian.raspberrypi.org/raspbian buster InRelease: The following signatures couldn't be verified because the public key is not available: NO_PUBKEY 9165938D90FDDD2E
E: The repository 'http://raspbian.raspberrypi.org/raspbian buster InRelease' is not signed.
N: Updating from such a repository can't be done securely, and is therefore disabled by default.
N: See apt-secure(8) manpage for repository creation and user configuration details.
```

```bash
sudo apt install gnupg2
sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 9165938D90FDDD2E
sudo apt update
```

```
All packages are up to date.
N: Skipping acquire of configured file 'main/binary-arm64/Packages' as repository 'http://archive.raspbian.org/raspbian buster InRelease' doesn't support architecture 'arm64'
N: Skipping acquire of configured file 'contrib/binary-arm64/Packages' as repository 'http://archive.raspbian.org/raspbian buster InRelease' doesn't support architecture 'arm64'
N: Skipping acquire of configured file 'non-free/binary-arm64/Packages' as repository 'http://archive.raspbian.org/raspbian buster InRelease' doesn't support architecture 'arm64'
```

Cannot find raspbian repository for arm64.

```bash
sudo apt install wget
sudo wget http://goo.gl/1BOfJ -O /usr/local/bin/rpi-update
sudo chmod +x /usr/local/bin/rpi-update
sudo rpi-update
```

```
/usr/local/bin/rpi-update: line 29: vcgencmd: command not found
/usr/local/bin/rpi-update: line 84: curl: command not found
 *** Raspberry Pi firmware updater by Hexxeh, enhanced by AndrewS and Dom
 *** Performing self-update
/usr/local/bin/rpi-update: line 95: curl: command not found
 !!! Failed to download update for rpi-update!
 !!! Make sure you have ca-certificates installed and that the time is set correctly
```

Drop it for now

```bash
sudo apt install build-essential
make ffmpeg # Fails because of gcc option
```

Stop trying to build last version. Try to build the version that was built for
the other raspberry-pi

```
rm -rf ffmpeg

git checkout 037c3c1eab2601dc1e8fb329c2290eb2380acb3c
sudo apt install gcc-arm-linux-gnueabihf
make ffmpeg
```

