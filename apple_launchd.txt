launchd remplace crontab pour mac

Les jobs sont décrits par un fichier .plist.

Le fichier .plist peut-être stocké dans 3 dossiers
  - /Library/LaunchDaemons : lancé même si aucun utilisateur n'est connecté
  - /Library/LaunchAgents : lancé seulement si un utilisateur est connecté
  - $HOME/Library/LaunchAgents : seulement si un utilisateur ($USER ?) est
      connecté (l'utilisater concerné ?)

Notes :
  Si le .plist est dans /Library, le script sera lancé par root. Attention à
    ce qu'il fait et attention aussi aux droits d'accès des fichiers nécessaire
    (dont le script) et des fichiers créés (s'ils doivent être édités)

Exemple de fichier .plist
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN"
  "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
<dict>
  <key>Label</key>
  <string>com.alvin.crontabtest</string>

  <key>ProgramArguments</key>
  <array>
    <string>/Users/al/bin/crontab-test.sh</string>
  </array>

  <key>Nice</key>
  <integer>1</integer>

  <key>StartInterval</key>
  <integer>60</integer>

  <key>RunAtLoad</key>
  <true/>

  <key>StandardErrorPath</key>
  <string>/tmp/AlTest1.err</string>

  <key>StandardOutPath</key>
  <string>/tmp/AlTest1.out</string>
</dict>
</plist>

 - Le label est le nom du job, il est conseillé de respecter les normes de
     nommage
 - ProgramArguments est le script à lancer
 - Nice ? (Peut-être une priorité)
 - StartInterval, la periode de lancement : toutes les 60 secondes
 - RunAtLoad : (A vérifier) Lancer le job quand il est chargé, ce qui n'est pas
     systématique
 - StandardErrorPath : Fichier où sera redirigé la sortie erreur
 - StandardOutPath : Fichier où sera redirigé la sortie sandard

$ launchctl list : Pour lister les jobs actifs, seulement pour $USER.

Pour lister les jobs de (tous les utilisateurs, y compris ??) root :
  # launchctl list

Pour charger un job : launchctl load com.alvin.crontabtest.plist

Pour stopper un job : launchctl unload com.alvin.crontabtest.plist
Note : Le job sera redémarré au redémarrage du système. Pour l'arrêter
  définitivement, il faut utiliser le chemin absolu du fichier .plist

Si le .plist est modifié alors que le job est chargé, les modifications ne
  sont pas repercutées, il faut le décharger et le recharger. Il n'y a pas non
  plus de messages d'erreur si le script n'existe pas.

Les sorties sont concaténées aux fichiers spécifiés comme des fichiers de
  logs, pas besoin de les déplacer.

Pour exécuter le script à dates fixe :
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN"
  "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
<dict>
    <key>Label</key>
    <string>com.example.happybirthday</string>
    <key>ProgramArguments</key>
    <array>
        <string>happybirthday</string>
    </array>
    <key>StartCalendarInterval</key>
    <dict>
        <key>Day</key>
        <integer>11</integer>
        <key>Hour</key>
        <integer>0</integer>
        <key>Minute</key>
        <integer>0</integer>
        <key>Month</key>
        <integer>7</integer>
        <key>Weekday</key>
        <integer>0</integer>
    </dict>
</dict>
</plist>

Le script happyB. sera lancé tous les dimanche 11 juillet à minuit
