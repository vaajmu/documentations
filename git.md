# Git

## Show file at some version

```bash
git show <branch>:<file>
```

## Show staged changes

```bash
git diff --cached
```

## Tags

### Lightweight tags

List tags:

```bash
git tag
```

Create tag on current commit:

```bash
git tag <tag name>
```

Create tag on a commit:

```bash
git tag <tag name> <commit ref>
```

Push tag on remote:

```bash
git push <origin> <tag name> # Push <tag name>
git push <origin> --tags     # Push all tags
```

Checkout a tag:

```bash
git checkout <tag name>
```

Delete a local tag:

```bash
git tag --delete tagname
```

Delete a distant tag:

```bash
git push --delete origin tagname
```

Print the current revision, in the form `<current tag>` if the commit is a tag
or `<last tag>-<nr of commits from last tag>-<commit hash>` if this commit is
not a tag or `<hash>` if there is no tags ih the history.

```bash
git describe --tags --always
```

## Submodules

### Add a submodule to a project

From (https://git-scm.com/book/en/v2/Git-Tools-Submodules)

```bash
cd <git parent project>
git submodule add [-b branch] https://github.com/chaconinc/DbConnector [<subdirectory name>]
```

If there is several URLs to access the subproject, use one accessible by
all. For example, a HTTPS public URL and a git URL who need identification, use
the HTTPS URL. To use the git URL locally, use the config command:

```bash
git config submodule.DbConnector.url PRIVATE_URL
```

### Clone a project with a submodule

First way:

```bash
git clone https://github.com/chaconinc/MainProject
cd MainProject
ls # Prints the DbConnector directory
ls DbConnector # The directory is empty
git submodule init
git submodule update
```

Second way:

```bash
git clone --recurse-submodule https://github.com/chaconinc/MainProject
cd MainProject
ls # Prints the DbConnector directory
ls DbConnector # The module is cloned
```

### Update a submodule

First way

```bash
cd <submodule directory>
git fetch
git merge origin/master
```

Second way

```bash
git submodule update --remote <submodule name>
```

Third way

```bash
cd <submodule dir>
git pull

cd <main repo>
git add <submodule dir>
git commit
```

To check the updates

```bash
git diff --submodule # ???
```

## Merge

```bash
sudo apt-get install kdiff3 # 'diffuse' is not that good
git am -3 00* # Use all patch files

# Conflicts detected
git mergetool [<files>]
# Confirm usage of kdiff3
# kdiff3 can resolve conflicts for you

# Check result
git diff HEAD [-- <files>]

# Accept changes
git am --continue
```

The `am` option `-3` allows to fix conflicts when they appear instead of just
rejecting the patch.

## Merge conflicts

Show the conflicting patch:

```bash
git am --show-current-patch
```

## Branches

```bash
# Remove branch
git br -D <branch>

# Remove on remote banches deleted locally
git push <origin> --delete <branch>

# Revove locally branches deleted on remote
git fetch <origin> --prune
```

## Git Worktree

Git worktree allow to clone the project once but share the `.git` directory
between multiple workspaces. This is usefull when the project is big to
prevent cloning the project multiple times to work with severa branches.

```bash
# Create a new workspace with a new branch named <name>
git worktree add <path/to/workspace/name>

# Create a new workspace using an existing branch
git worktree add <path> <branch>

# Create a new workspace using current commit (will be in a detached state)
git worktree add -d <path>

# When we are done with the workspace
git worktree remove <path>

# Clean up stale working tree administration files
# In the main repo or in a working tree
# Usefull when a working tree was removed without 'remove'
git worktree prune

# List main repo and all working trees
git worktree list
```

## Commit a fixup

```bash
git commit --fixup=<old commit>
git rebase --autosquash --interactive <old commit>^
```

## Stash only some files

Example of commands:

```bash
# Multiple files
git stash push -m "message" <file>...

# Only part of a file
git stash push -p -m "message" <file>
```

## Check if a branch contains a commit

```bash
git branch --contains <commit id>
```

## Clean untracked files

Remove files that are not tracked AND that are not in the .gitignore file

```bash
git clean -n # See which files will be deleted
git clean -f # Remove the files

# Also remove untracked directories
git clean -nd
git clean -fd
```
## Partial add of an untracked file

```bash
git add --intent-to-add <new_file>
git add -p <new_file>
```

## Extract part of a patch in a separate patch

Note: this should be tested.

```bash
git rebase --interactive <commit>^
git revert --no-commit -p HEAD
# Add and remove things from stashed changes
# Remove from stages changes what should stay in the patch
# Then remove them also from the un-staged changes
# Leave in the staged changes what should not be in this patch
git commit --amend
git cherry-pick <commit> # Initial commit before changes
# Edit the new patch if necessary and update the commit message
git rebase --continue
```
