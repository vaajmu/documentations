# How to use PlantUML

## In emacs

Emacs25 or superior is required, it won't works with Emacs24

```bash
emacs /tmp/test.txt
M-x package-install RET plantuml-mode RET
M-x plantuml-mode RET
```

Add in `~/.emacs`:

```
;; If you downloaded the JAR file from the website
;; Sample jar configuration
(setq plantuml-jar-path "/path/to/your/copy/of/plantuml.jar")
(setq plantuml-default-exec-mode 'jar)

;; If you installed a shell script ('apt install' does that)
;; Sample executable configuration
(setq plantuml-executable-path "/path/to/your/copy/of/plantuml.bin")
(setq plantuml-default-exec-mode 'executable)
```

Add text in the buffer:

```
@startuml
Alice -> Bob: Authentication Request
Bob --> Alice: Authentication Response

Alice -> Bob: Another authentication Request
Alice <-- Bob: Another authentication Response
@enduml
```

Use C-c C-c to render the code.
