# Build a system for the Raspberry Pi

## Build an image for qemu

```bash
cd ~/workspace
mkdir rpi-yocto
cd rpi-yocto
git clone -b dunfell git://git.yoctoproject.org/poky
cd poky
source oe-init-build-env qemux86-build
bitbake core-image-minimal
```

```
ERROR: Error running gcc  --version: ccache: error: Could not find compiler "gcc" in PATH
```

```bash
export CC=/usr/bin/gcc
export CXX=/usr/bin/g++
bitbake core-image-minimal # Still fails
export CCACHE_PATH="$PATH"
bitbake core-image-minimal # Still fails
```

ccache was installed by creating symbolic links in ~/bin.

```bash
cd ~/bin
mkdir ccache-links
mv cc c++ gcc g++ ccache-links
cd ~/workspace/rpi-yocto/poky/qemux86-build
bitbake core-image-minimal

cd ~/workspace
mkdir rpi-yocto
cd rpi-yocto
git clone -b dunfell git://git.yoctoproject.org/poky
cd poky
source oe-init-build-env qemux86-build
bitbake core-image-minimal
```

```
ERROR: Error running gcc  --version: ccache: error: Could not find compiler "gcc" in PATH
```

```bash
export CC=/usr/bin/gcc
export CXX=/usr/bin/g++
bitbake core-image-minimal # Still fails
runqemu qemux86
```

```
runqemu - INFO - Running MACHINE=qemux86 bitbake -e ...
runqemu - ERROR - /home/anloh/workspace/rpi-yocto/poky/qemux86-build/tmp/deploy/images/qemux86 not a directory valid DEPLOY_DIR_IMAGE
ls: cannot access '/home/anloh/workspace/rpi-yocto/poky/qemux86-build/tmp/deploy/images/qemux86/*.qemuboot.conf': No such file or directory
runqemu - ERROR - Command 'ls -t /home/anloh/workspace/rpi-yocto/poky/qemux86-build/tmp/deploy/images/qemux86/*.qemuboot.conf' returned non-zero exit status 2
runqemu - INFO - Cleaning up
```

```bash
runqemu --help
runqemu qemux86-64
```

## Build an image for the Raspberry Pi

```bash
cd ~/workspace/rpi-yocto/poky
git clone -b dunfell git://git.yoctoproject.org/meta-raspberrypi
ls meta-raspberrypi/conf/machine/ # List available machines
source oe-init-build-env rpi3-build
echo "MACHINE = \"raspberrypi3\"" >> conf/local.conf
bitbake-layers add-layer ../meta-raspberrypi
bitbake core-image-minimal
```

## Flash SD Card

```bash
sudo dd if=tmp/deploy/image/raspberrypi3/core-image-minimal-raspberrypi3.rpi-sdimg of=/dev/mmcblk0
```

## Configure IPK

Edit file `conf/local.conf`

```
EXTRA_IMAGE_FEATURES += "package-management"
PACKAGE_CLASSES = "package_ipk"
```

## Build qemu image in the raspberry build directory

In the environment configured to build the Raspberry Pi image, use:

```bash
MACHINE="qemux86-64" bitbake core-image-minimal
```

Nothing more in `tmp/deploy/images`.

Edit file conf/local.conf and remove (comment) the line `MACHINE`

```bash
bitbake core-image-minimal
```

