# How to install wifi drivers

On debian stretch/testing

## Identify the device

`lspci` is given by `pciutils` package (installed by default)

`hardinfo` is available on `gnome`

`kinfocenter` is available on `KDE`

```bash
lspci
```

Gives :

```
[...]
02:00.0 Network controller [0280]: Intel Corporation Wireless 7260 [8086:08b1] (rev c3)
[...]
```

## Search for micro code

Google `Intel Corporation Wireless 7260` and find intel website for Linux
support.

```bash
cd ~/Téléchargements
tar xf iwlwifi-7260-ucode-25.30.14.0.tgz
cd iwlwifi-7260-ucode-25.30.14.0/
sudo cp iwlwifi-7260-14.ucode /lib/firmware/
ip a # Doesn't show wifi
```

Reboot and try again

```bash
ip a # Still doesn't work
```

## Try with non-free repository

Try otherwise, search for `8086:08b1` with google. This page looks useful :

https://wiki.debian.org/fr/iwlwifi

Update `/etc/apt/sources.list` and add :

Add `contrib non-free` at the end of lines `deb` and `deb-src` without
`security`

For example, my file after :

```
deb http://ftp.lip6.fr/pub/linux/distributions/debian/ testing main contrib non-free
deb-src http://ftp.lip6.fr/pub/linux/distributions/debian/ testing main contrib non-free
deb http://security.debian.org/debian-security testing/updates main
deb-src http://security.debian.org/debian-security testing/updates main
```

```bash
# Update repo
sudo apt-get update
# Install package
sudo apt-get install firmware-iwlwifi
# Restart module iwlwifi
sudo modprobe -r iwlwifi
sudo modprobe iwlwifi

ip a # Show wifi
```
