# How to swap keyboard keys

## Test 1

### Find keyboard keycodes

```bash
xev
```

This program starts a blank windows. When the window is focused all
mouse/keyboard events are logged in the terminal.

We need to extract the keycode and the action of the keys we are swapping.

Example for the 'Fn' key and the 'Ctrl' key:

```
KeyPress event, serial 32, synthetic NO, window 0x3c00001,
    root 0x3fd, subw 0x0, time 2294617, (1226,586), root:(1227,606),
    state 0x0, keycode 151 (keysym 0x1008ff2b, XF86WakeUp), same_screen YES,
    XLookupString gives 0 bytes: 
    XmbLookupString gives 0 bytes: 
    XFilterEvent returns: False

KeyRelease event, serial 32, synthetic NO, window 0x3c00001,
    root 0x3fd, subw 0x0, time 2294627, (1226,586), root:(1227,606),
    state 0x0, keycode 151 (keysym 0x1008ff2b, XF86WakeUp), same_screen YES,
    XLookupString gives 0 bytes: 
    XFilterEvent returns: False

KeyPress event, serial 32, synthetic NO, window 0x3c00001,
    root 0x3fd, subw 0x0, time 2295012, (1226,586), root:(1227,606),
    state 0x0, keycode 37 (keysym 0xffe3, Control_L), same_screen YES,
    XLookupString gives 0 bytes: 
    XmbLookupString gives 0 bytes: 
    XFilterEvent returns: False

KeyRelease event, serial 32, synthetic NO, window 0x3c00001,
    root 0x3fd, subw 0x0, time 2295072, (1226,586), root:(1227,606),
    state 0x4, keycode 37 (keysym 0xffe3, Control_L), same_screen YES,
    XLookupString gives 0 bytes: 
    XFilterEvent returns: False
```

Which gives:

* Fn key keycode: 151
* Fn key action: XF86WakeUp
* Ctrl key keycode: 37
* Ctrl key action: Control_L

### Swap th keys

```bash
xmodmap -e "keycode 151 = Control_L"
xmodmap -e "keycode 37 = XF86WakeUp"
```

### Check result

Start again `xev` and ckeck that hitting a key gives the same key than before
but the action is now changed.

Even though this works, using the keys in programs does not seem impacted :(

## Test 2

From
[https://help.ubuntu.com/community/Custom%20keyboard%20layout%20definitions?action=show&redirect=Howto:+Custom+keyboard+layout+definitions]

Look file `/usr/share/X11/xkb/keycodes/evdev` and get key code (the one between
"<>") from the keycodes (the one retrived with xev).

This gives :

* Fn key -> 151, <I151>, KEY_WAKEUP
* Ctrl key -> 37, <LCTL>

Get keyboard layout :

```bash
setxkbmap -query
```

```
rules:      evdev
model:      pc105
layout:     fr
variant:    latin9
```

Edit file `/usr/share/X11/xkb/symbols/fr`. Using the information returned by the
`setxkbmap -query` command, whe should edit the `xkb_symbols "latin9"` section

Note that this files list the symbols printed using the 'AltGr' key.

This won't work ! The Fn key is special.

The BIOS have a keyoard menu and allows to swap both keys. This works. Forget
about Linux configuration.

