## Convert PDF to JPG

```bash
convert -colorspace RGB -interlace none -density 300x300 -quality 100 file.pdf file.jpg
```
