# LibreOffice Impress (Slides)

## Insert videos in a slide

When exporting the document to pdf, the document is only displaying a photo
and doesn't allow to play the video.

From https://bugs.documentfoundation.org/show_bug.cgi?id=105093

```bash
pdftk presentation.pdf output pres-with-pdftk.pdf
```

Evince can now play the video.

Evince cannot play the video in the presentation mode.

Bugreport here: https://gitlab.gnome.org/GNOME/evince/-/issues/869