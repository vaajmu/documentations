# Create presentation using Markdown

Download revealjs 3.9 from
https://codeload.github.com/hakimel/reveal.js/tar.gz/refs/tags/3.9.2 and
extract it soewhere.

Install pandoc:

```
sudo apt install pandoc
```

Write the content in markdown then convert it to HTML:

```
pandoc --to=revealjs \
       --variable=revealjs-url:<path to revealjs> \
       --standalone \
       slides.md \
       --output=slides.html
```

Option `--self-contained` can be used instead of `--standalone` to make an
autonomous single page.
