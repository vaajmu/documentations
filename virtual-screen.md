# Create a virtual screen

```bash
sudo apt-get install xserver-xorg-video-dummy
```

Edit file `/usr/share/X11/xorg.conf.d/xorg.conf` (or `/etc/X11/xorg.conf`?).

The file dos not exists so create it.

Insert this content:

```
Section "Device"
    Identifier  "Configured Video Device"
    Driver      "dummy"
EndSection

Section "Monitor"
    Identifier  "Configured Monitor"
    HorizSync 31.5-48.5
    VertRefresh 50-70
EndSection

Section "Screen"
    Identifier  "Virtual Screen"
    Monitor     "Configured Monitor"
    Device      "Configured Video Device"
    DefaultDepth 24
    SubSection "Display"
    Depth 24
    Modes "1024x800"
    EndSubSection
EndSection
```

Restart X.

It blocks when starting. Force poweroff. Remove file before starting X again.
