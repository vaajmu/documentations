# Force clock update

```date
sudo apt-get install ntpdate
sudo service ntp stop
sudo ntpdate -s time.nist.gov
sudo service ntp start
```

# Configure NTP server

```bash
sudo apt-get install ntp
```

Edit file `/etc/ntp.conf` and add `restrict 192.168.198.0 mask 255.255.255.0
nomodify notrap` lines if needed. This line will allow 192.168.198.* clients to
connects to this server.

