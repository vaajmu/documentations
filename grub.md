# Start existing system from Grub commandline

If Grub is not start at boot, start a live image. When the grub menu is
displayed, use the 'c' key to enter the commandline menu. Then use the
command configfile to start on a specified grub:

```
background_image # Remove the background image
configfile (hd1,msdos1)/grub/grub.cfg
```

