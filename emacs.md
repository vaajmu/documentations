# Get current buffer file path

```
M-: buffer-file-name
```

# Don't indent curly braces when at the beginning of a line

## Using configuration file

```
;; in ~/.emacs
(setq c-default-style "bsd"
    c-basic-offset 4)
```

## In the current buffer

```
M-x c-set-style bsd
```

# Set max line length for text

```
M-x set-variable
fill-column
<max line length>
```

# Set tabs spaces nr for C mode

## With config file

Add to configuration file:

```lisp
(setq-default c-basic-offset 4)
```

## Temporarilly change

Enter `M^x`, `set-variable`, enter, `c-basic-offset`, enter, `4`. This will
configure the tab **key** as 4 spaces.

Enter `M^x`, `set-variable`, enter, `tab-width`, enter, `4`. This will
configure the tab **char** as 4 spaces.

# Select between tabs and spaces in a buffer

## Configure emacs

Edit ~/.emacs and add:

```
(defun disable-tabs ()
  (interactive)
  (setq indent-tabs-mode nil))

(defun enable-tabs  ()
  (interactive)
  (local-set-key (kbd "TAB") 'tab-to-tab-stop)
  (setq indent-tabs-mode t)
  (setq tab-width custom-tab-width))
```

Then restart emacs

## Switch configuration

* When using spaces, switch to tabs with the command `enable-tabs`
* When using tabs, switch to spaces with the command `disable-tabs`

# Remove trailing whitespaces

## With config file

```lisp
(add-hook 'before-save-hook 'delete-trailing-whitespace)
```

## Temporarilly change

`M^x delete-trailing-whitespace`

# Replace tabs with spaces

`C^x+h M^x untabify`

## Use tags

```bash
sudo apt-get install exuberant-ctags
cd <path to project>
ctags -e -R
```

If emacs was using an other file, use command `M-x visit-tags-table` and give
the TAG file path.

Then :

* `M-.’ (‘find-tag’) – find a tag, that is, use the Tags file to look up a
  definition. If there are multiple tags in the project with the same name, use
  `C-u M-.’ to go to the next match.

* ‘M-x find-tag-other-window’ – selects the buffer containing the tag’s
  definition in another window, and move point there.

* ‘M-*’ (‘pop-tag-mark’) – jump back

* ‘M-x tags-search’ – regexp-search through the source files indexed by a tags
  file (a bit like ‘grep’)

* ‘M-x tags-query-replace’ – query-replace through the source files indexed by a
  tags file

* `M-,’ (‘tags-loop-continue’) – resume ‘tags-search’ or ‘tags-query-replace’
  starting at point in a source file

* ‘M-x tags-apropos’ – list all tags in a tags file that match a regexp

* ‘M-x list-tags’ – list all tags defined in a source file

## Revert 'read only buffer'

`M-x read-only-mode`

## Alternative to ctags

`cscope` can be used to navigate C/C++ code.

tutoriels can be found :

* (https://techtooltip.wordpress.com/2012/01/06/how-to-integrate-emacs-cscope-to-browse-linux-kernel-source-code/)

## Others alternatives

* (https://emacs.stackexchange.com/questions/9499/emacs-cscope-integration-basics)

```bash
sudo apt-get install cscope
sudo apt-get install xcscope-el
ls /usr/share/emacs/site-lisp/xcscope
```

Edit `~/.emacs` and add

```
(require 'xcscope)
```

Bindings:

```
C-c s s Find symbol.

C-c s d Find global definition.
C-c s g Find global definition (alternate binding).
C-c s G Find global definition without prompting.
C-c s c Find functions calling a function.
C-c s C Find called functions (list functions called
    from a function). 
C-c s t Find text string.
C-c s e Find egrep pattern.
C-c s f Find a file.
C-c s i Find files #including a file.

    These pertain to navigation through the search results:

C-c s b Display *cscope* buffer. 
C-c s B Auto display *cscope* buffer toggle.
C-c s n Next symbol.
C-c s N Next file.
C-c s p Previous symbol.
C-c s P Previous file.
C-c s u Pop mark.

    These pertain to setting and unsetting the variable, `cscope-initial-directory', (location searched for the cscope database directory):

C-c s a Set initial directory. 
C-c s A Unset initial directory.

    These pertain to cscope database maintenance:

C-c s L Create list of files to index. 
C-c s I Create list and index.
C-c s E Edit list of files to index.
C-c s W Locate this buffer's cscope directory
    ( "W" --> "where" ). 
C-c s S Locate this buffer's cscope directory.
    (alternate binding: "S" --> "show" ). 
C-c s T Locate this buffer's cscope directory.
    (alternate binding: "T" --> "tell" ). 
C-c s D Dired this buffer's directory. 
```

Try it. Open a source file and make a search with `C-c s d`. No results because
the definition is in a parent folder and the cscope database is created in the
source file folder. Use `C-c s a` and specify the sources root directory. Now
try again `C-c s d`: `cscope: no source files found`. Use `C-c s L` with the
sources root directory and `C-c s d`. It works.

## Start with a new project and xcscope

```
C-c s a # Set sources root directory
C-c s L # Create list of files to index
C-c s d # Symbol search
# Search show errors 'cscope: cannot find file ...'
C-c s I # Create list AND index
C-c s d # Succeed
```

From [this page](http://cscope.sourceforge.net/large_projects.html)

By default cscope uses *.c, *.h, *.l and *.y files but sometimes we want to
exclude a directory or include another extentions. The file `cscope.files`
contains all files to be used by cscope. It can be generated under emacs with
`C-c s I`. The command `find` can be used to better manage the files to read:

```bash
find . \
     -path "build" -prune -o \
     -name "*.[chxsS]" \
     -print > cscope.files
```

Another example:

```bash
find . \
     ! \( -name "*.blk.c" \) -a \
     -name "*.[chxsS]" -o \
     -name "*.blk" > cscope.files
```

When the file is updated run the following command to update the database:

```bash
cscope -b -q
```

This first time a warning can be displayed. To be sure remove the previous
database (cscope.out) and run again the command.

In emacs we need to reload the database without regenerating the file. This is
done withe the `C-c s A`. Don't use `C-c s I` or it will override the file we
just updated

# Disable auto-indent in text mode

When the text mode is enabled and the curent line contains spaces/tabs at the
begining, pressing Enter will insert the same number of spaces/tabs on the new
line. This is annoying when pasting data/code from another program because all
indentations are cumulated.

We can temporarily disable this function using `M-x electric-indent-mode`

# Use the package manager

Edit ~/.emacs:

```
(require 'package)
(setq package-archives '(("Gnu" . "https://elpa.gnu.org/packages/")
                         ("Melpa"   . "https://melpa.org/packages/")))
```

Restart emacs and use `M-x package-refresh-contents RET` to update the local
package list.

Then use `M-x package-install RET <package name> RET`

# PHP mode

## web-mode

[https://web-mode.org/]

```bash
sudo apt install elpa-web-mode
```

Add in `~/.emacs`:

```
;; web-mode
(require 'web-mode)
(add-to-list 'auto-mode-alist '("\\.php\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.phtml\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.tpl\\.php\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.[agj]sp\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.as[cp]x\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.erb\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.mustache\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.djhtml\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.html?\\'" . web-mode))
```

# lsp

Langage Server Protocol

[https://langserver.org/]

[https://emacs-lsp.github.io/lsp-mode/page/installation/]

```bash
# Emacs client
sudo apt install elpa-lsp-mode

# PHP server
npm i intelephense -g
```

# Reload current buffer

When the file opened in the current buffer is modified, pressing any key will
suggest to reload the buffer with the new file's content.

When the current buffer is read-only this won't be proposed. To reload the
buffer, use:

```
M-x revert-buffer
```

# GDB inside emacs

[https://undo.io/resources/gdb-watchpoint/using-gdb-emacs/]

Start a program under GDB:

```
M-x gdb
gdb -i=mi <binary path>
```

Attach to a running program under GDB:

```
M-x gdb
gdb -i=mi -p <debugged process PID>
```

Open gdb-many-windows for a better view:

```
M-x gdb-many-windows
```

Maybe the wanted buffers are not displayed automatically, if not, got to the
buffer with `C-x o` and change it with `C-x b`. Usefull buffers are:

* stack frames
* locals
* input/output
* breakpoints
* threads
* gud

To restore the initial layout:

```
M-x gdb-restore-windows
```

When debug ends, kill the GUD bugger (gdb associated buffer??) with `C-x k`.

From
[https://www.gnu.org/software/emacs/manual/html_node/emacs/Commands-of-GUD.html]:

Shortcuts:

```
# Set a breakpoint on the current line:
C-x C-a C-b
M-x gud-break

# step, go to next line or enter function call if any
C-c C-s
M-x gud-step

# next, go to next line of code in the function
C-c C-n
M-x gud-next

# stepi, execute a single machine instruction
C-c C-i
M-x gud-stepi

# print, evaluate variable under cursor
#    note: mark the region if the wrong variable is displayed
C-c C-p
M-x gud-print

# run, continue without explicit stopping point
C-c C-r
M-x gud-cont

# delete, remove breakpoint
C-c C-d
M-x gud-remove

# until, continue execution until the current line
C-c C-u
M-x gud-until

# finish, run until the current frame ends
C-c C-f
M-x gud-finish

# Stop process
C-c C-c
```

Note that the shortcuts are valid in the GDB buffers, in other buffers (like
sources) the prefix `C-c` must be replaced with `C-x C-a`.

After a shortcut is used, in the gud buffer, hitting the enter key will apply
the shortcut again.

After editing the sources, build the program in the usual terminal, then in
the GUD/GDB emacs buffer:

```
# Interrupt the process if it is running
C-c C-c

# Kill the process
k

# If gdb was started with the program path
# Run again the process
r

# If gdb was started with the running program PID
# Restart your program yourself
# Then attach to it
attach <PID>
```

Change one of GUD's buffer (C-x o cannot be used with them):

```
M-x gdb-display-buffertype-buffer
```

`buffertype` is the name of the buffer to use

From
[https://www.gnu.org/software/emacs/manual/html_node/emacs/Shell-Ring.html]

In the gud buffer, previous command (resp. next command) can be retrieved
using `M-p` or `C-up` (resp `M-n` or `C-down`).

To search in the command history: `M-r <search-regex>`

# Check spelling

[https://www.gnu.org/software/emacs/manual/html_node/emacs/Spelling.html]

```
# Start flyspell mode (will highlight all mispelled words when written) :
M-x flyspell-mode

# Set lang if not english, before the flyspell-buffer command
M^x ispell-change-dictionary
francais

# Check current buffer (flyspell mode doesn't need to be started) :
M-x flyspell-buffer

# Check and correct current word (Same options than the ispell command below)
M-$

# Check and correct all buffer (or region if one is active)
M-x ispell
# If an error is detected:
# * A digit will replace the word with the suggestion
# * Space will ignore the word
# * '?' Display possible commands
# * 'a' Accept for this session
# * 'i' Insert this word in your private dictionary file
# * 'u' Insert the lower-case version of this word in your private dictionary file
```

Use another language

```bash
sudo apt install aspell-fr
```

```
M^x ispell-change-dictionary
francais
M^x flyspell-buffer
```

# Use regex to replace strings

```
M-x replace-regex
```

Parenthesis should be escaped for the matched-part to be used in the replaced
text.

## Example

Pattern:

```
0\([0-9]\)
```

Replacement:

```
\1
```

Will replace all digits prefixed with a `0` by the digit only.

# Import a new mode

Consider an `<filename>.el` file containing a `(define-generic-mode <mode>`,
place it somewhere.

Inside emacs, use `M-x load-file` the select the `<filename>.el` file

Now use it as a major mode: `M-x <name>`

# Configure max line length for commit messages

```
# Declare the new mode/name
(defvar git-mode-hook nil)

# Configure the new mode for file '.COMMIT_EDITMSG'
(add-to-list 'auto-mode-alist '("COMMIT_EDITMSG" . git-mode))

# Describe the new mode (including max line length)
(defun git-mode ()
  "Major mode for editing commit messages"
  (interactive)
  (kill-all-local-variables)
  (set-fill-column 72))

# Don't know if it is useful
(add-hook 'git-mode-hook (lambda () (set-fill-column 72)))
```

# 80 column marker

From https://www.emacswiki.org/emacs/ColumnMarker

Download column-marker.el at
https://www.emacswiki.org/emacs/download/column-marker.el

Place .el file under `~/.emacs.d`

In `~/.emacs` add line `(require 'column-marker)`

Still in `~/.emacs` make sure the directory is in the load path:

When emacs is started, use `M-: load-path` and check if the directory is
listed. If it is not, open `~/.emacs` and add this line: `(add-to-list
'load-path "<dir>")`

To enable marker on too-long lines: `C-u 80 M-x column-marker-1`

To configure default marker for a mode, add in `~/.emacs` the line `(add-hook
'foo-mode-hook (lambda () (interactive) (column-marker-1 80)))`

# Folds {{{ }}}

https://raw.githubusercontent.com/jaalto/project-emacs--folding-mode/master/folding.el

Save the .el file in the load-path (see § `80 column marker` if needed)

In `~/.emacs` add lines:

```
(autoload 'folding-mode          "folding" "Folding mode" t)
(autoload 'turn-off-folding-mode "folding" "Folding mode" t)
(autoload 'turn-on-folding-mode  "folding" "Folding mode" t)
```

* To enable mode   : `M-x folding-mode`. This will wrap all folds.
* Enter a fold     : `C-c @ >`
* Leave a fold     : `C-c @ >`
* Enter/exit fold  : `C-c @ C-u`
* Show a fold      : `C-c @ C-s`
* Open all folds   : `C-c @ C-o`
* Close all folds  : `M-x folding-whole-buffer`
* Toggle fold      : `C-c @ C-q`

# Resize window

Split buffer at 78 chars:

```
M-: (split-window-horizontally 80)
```

Note sure why the 2 extra columns

# Macro backslashes

https://www.gnu.org/software/emacs/manual/html_node/ccmode/Macro-Backslashes.html

## Add backslashes for a region

To convert a function (or a block of code) into a macro, select the region and
use `M-x c-backslash-region`.

# Modify files from emacs

Rename file:

Open dired buffer with `M-x dired <RET>`

* Use `R` to rename a file
* Use 'D' to delete a file

# Highlight symbols V1

```
M-x highlight-symbol-at-point # Highlight symbol the cursor is on
C-u C-x w r # Unhighlight everything
```

## Configure key binding

In `.emacs`:

```
(global-set-key (kbd "<f3>") 'highlight-symbol-at-point)
```

# Highlight symbols V2

Download Highlight https://www.emacswiki.org/emacs/download/highlight.el.

Place it in `~/.emacs.d` somewhere. Make sure this directory is in the load
path (`(add-to-list 'load-path "~/.emacs.d/eldir/")`).

Restart emacs.

```
C-x X h s     # Highlight a symbol
C-x X u s     # Un-highlight a symbol
```