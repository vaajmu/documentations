# Install and configure TFTP server

```bash
sudo apt-get install xinetd tftpd tftp
```

Edit `/etc/xinetd.d/tftp` and add:

```
service tftp
{
protocol        = udp
port            = 69
socket_type     = dgram
wait            = yes
user            = nobody
server          = /usr/sbin/in.tftpd
server_args     = /tftpboot
disable         = no
}
```

```bash
sudo mkdir /tftpboot
sudo chmod -R 777 /tftpboot
sudo chown -R $USER /tftpboot
sudo /etc/init.d/xinetd reload # Doesn't work
sudo apt-get install tftpd-hpa -y
sudo /usr/sbin/in.tftpd -l &
```

```bash
tftp localhost
get test.txt # Error code 2: Only absolute filenames allowed
get /test.txt # Error code 2: Forbidden directory
get /tftpboot/test.txt # Works
```

Remove file /etc/xinet.d/tftp. Use options `-l` for the server for it to wait a
connection.

Server option `-s` mays allow to use relative path (without /tftpboot in
request).

Server option `-L` keep the server in foreground and allow to kill it using
`Ctrl+c` instead of using POSIX signals.

```bash
ps -e | \grep ftp # One process found
sudo pkill in.tftpd
ps -e | \grep ftp # No process found
sudo /usr/sbin/in.tftpd -L -l
```

In another terminal:

```bash
echo "get /tftpboot/test.txt" | tftp localhost
```

## Try to remove the absolute path in the get command

```bash
sudo /usr/sbin/in.tftpd -L -l --secure # Fails
sudo /usr/sbin/in.tftpd -L -l -s /tftpboot
```

In another terminal:

```bash
echo "get test.txt" | tftp localhost
```

> It works !

Disable autostart

```bash
sudo update-rc.d -f tftpd-hpa remove
```