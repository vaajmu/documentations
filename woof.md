# Install woof

From repo

```bash
sudo apt install woof
```

From the web

```bash
wget http://www.home.unix-ag.org/simon/woof
sudo cp woof /usr/bin/
```
