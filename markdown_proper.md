# How to use markdown

## How to write a document

## How to make a PDF file

### Install gimli

```bash
sudo apt-get install rubygems-integration wkhtmltopdf ruby-dev
sudo gem install gimli
```

### Convert `md` file to `pdf`

Note : Converted file **must** end with `.md`. Otherwise it won't be converted

```bash
# With no file given, gimli convert all .md files in the directory
gimly

# Convert a file
gimli -f <md file>

# Use CSS file
gimli -f <md file> -stylesheet <css file>

# Use CSS and add a summary
gimli -f <md file> -stylesheet <css file> -w '-t'

# Print HTML file. Allow to work on CSS
gimli -f <md file> -debug
```

### `Gimli` options

Markdown documents are converted to HTML with `gimli` and then to PDF with `wkhtmltopdf`

`gimli -h` prints gimli help

`wkhtmltopdf -h` prints whhtmltopdf help

You can give wkhtmltopdf argiments to gimli with the `-w` option. Those options
should be in quotes to not be mixed with gimli options.

