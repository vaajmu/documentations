Pour Afficher une notification avec "Yada yada yada" :
osascript -e 'tell app "System Events" to display dialog "Yada yada yada"'

Pour Afficher une notification dans le finder (fait sautiller l'icone jusqu'à
  ce que l'utilisateur clique dessus)
osascript -e 'tell app "Finder" to display dialog "Yada yada yada"'

