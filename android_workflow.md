# How to develop with Android

[Source](http://blog.udinic.com/2014/07/24/aosp-part-3-developing-efficiently/)

## Flash device

### Way #1

adb reboot bootloader  // Put the device in fastboot mode
fastboot flashall -w      // Flashes the images created

### Way #2

Open a minicom on the device, stop and start device and stop boot using space
key. Then use `fastboot` command to start fastboot daemon on the device

In an other terminal, go to the Android source directory, use environment.sh and
lunch, then

```bash
fastboot flash boot out/target/product/var_mx6/boot-som-solo-vsc.img
fastboot flash recovery out/target/product/var_mx6/recovery-som-solo-vsc.img
fastboot flash system out/target/product/var_mx6/system.img
fastboot reboot
```

### Way #3

```bash
# The first time:
mount -o rw,remount /system

# Then:
adb sync

# Only if frameworks/base was modified:
adb shell stop; adb shell start
```

## Build repo

### Way #1

```bash
croot
make -j8
```

### Way #2

```bash
m -j8
```

### Way #3

```bash
mm -j8 <module name>
```


