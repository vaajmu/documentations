
## Input events

List every key change, mouse too:

```
wev
sudo libinput debug-events
```

## Auto rotate screen

```
# Print dbus notifications
dbus-monitor --system

# Print node
gdbus introspect --system --dest net.hadess.SensorProxy --object-path /net/hadess/SensorProxy

# Get screen current orientation
dbus-send --system --print-reply --dest=net.hadess.SensorProxy /net/hadess/SensorProxy org.freedesktop.DBus.Properties.Get string:net.hadess.SensorProxy string:AccelerometerOrientation

# Watch orientation change
dbus-monitor --system --profile "interface='org.freedesktop.DBus.Properties',member='PropertiesChanged'"
```

## Configure displays

Install and start `wdisplays` to configure the monitors using a GUI.

List displays:

```
ls /sys/class/drm/
```

List one displays available modes (resolutions):

```
cat /sys/class/drm/card0-HDMI-A-1/modes
```

Other way of getting display's infos:

```
swaymsg -t get_outputs
```

```
# TODO configure screens
```

## Disable screen

```bash
swaymsg -t get_outputs
swaymsg "output eDP-1 disable"
```
