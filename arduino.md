
# Install Ino

Ino can replace Arduino IDE. Arduino doesn't work properly with DWM and can't be
used from command line. Ino can.

[Ino website](http://inotool.org/)

```bash
pip install ino
sudo apt-get install picocom
sudo apt-get install arduino
python --version # OK
ino help # Fails
pip ino help # Fails
python ino help # Fails
sudo pip install ino
which ino # Works !
```

# Use Ino

(http://inotool.org/quickstart)

## Create project

```bash
cd <wokspace>
mkdir <project name>
ino init
```

## Link to a libreary

```bash
cd lib
ln -s <library path> .
```

Edit `<project name>/src/sketch.ino`

## Build project

```bash
cd <workspace>/<project name>
ino build
cd src
ino build # Fails, must be in the project's root directory
```

## Upload project

```bash
ino upload
```

```
Searching for stty ... /bin/stty
Searching for avrdude ... /usr/share/arduino/hardware/tools/avrdude
Searching for avrdude.conf ... /usr/share/arduino/hardware/tools/avrdude.conf
Guessing serial port ... /dev/ttyUSB0
/bin/stty: /dev/ttyUSB0: Permission denied
stty failed
```

```bash
sudo ino upload # Success
```

## Get serial communication

```bash
ino serial # Fails
sudo ino serial # Connects but garbage is printed
# Quit with Ctrl+a Ctrl+x
ino seria help # Nope
ino serial --help
ino serial -b 115200 # Success
```

## Configure Ino

```bash
cd <workspace>/<project name>
echo "baud-rate = 115200" >> ino.ini
sudo ino serial # Does not print garbage
```

