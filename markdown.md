# Convert Markdown files to PDF

This website looks useful :

https://kevin.deldycke.com/2012/01/how-to-generate-pdf-markdown/

## Try Gimli

```bash
sudo apt-get install rubygems-integration wkhtmltopdf
sudo gem install gimli
```

```
ERROR:  Error installing gimli:
        ERROR: Failed to build gem native extension.

    current directory: /var/lib/gems/2.3.0/gems/redcarpet-3.4.0/ext/redcarpet
    /usr/bin/ruby2.3 -r ./siteconf20171214-19515-1aj7vl5.rb extconf.rb
    mkmf.rb can't find header files for ruby at /usr/lib/ruby/include/ruby.h
```


```bash
sudo apt-get install ruby # Nothing done
sudo apt-get install gem2dep # Dependencies error
sudo apt-get update && sudo apt-get -y upgrade
sudo apt-get install gem2dep # Still dependencies error
sudo apt-get install ruby2.5-dev
sudo apt-get purge ruby ruby2.3 ruby2.5-dev
sudo apt-get install ruby2.5*
sudo apt-get install rubygems-integration ruby-dev
sudo gem install gimli # Works
```
 
```bash
gimli -f myfile.txt # doesn't work but no errors
gimli -f myfile.md # create a PDF
```
 
## Bash highlighting

Bash is not highlighted

Gimli uses `coderay` to highlight text. Maybe it is not installed right.

```bash
sudo gem install coderay
```

Command succeed but there is still no highlight for bash.

From [coderay website](http://coderay.rubychan.de/), bash is not supported.


## Try with `markdown-pdf`

After some research on github,
[markdown-pdf](https://github.com/alanshaw/markdown-pdf) might be good

### Installation

#### Install npm

mhhh ... nope

#### Cloning repo

```bash
git clone https://github.com/alanshaw/markdown-pdf.git
```

### Try `markdown-pdf`

```bash
cd markdown-pdf/bin
./markdown-pdf <.md file> -o ./test.pdf
```

Fails with error `/usr/bin/env: ‘node’: No such file or directory`

Too bad !

## Try again with `pandoc`

The first website said that `pandoc` wasn't great because the lines do not wrap
in code blocks. Let's try ourselves

[pandoc github](https://github.com/Wandmalfarbe/pandoc-latex-template)

see http://pandoc.org/MANUAL.html#syntax-highlighting for highlighting 

### Installation

Pandoc provide `.deb` file from thir website. It should be less outdated than
debian repository. Download and install it

```bash
sudo dpkg -i /home/hiesoo/Templates/pan
```

Download `eisvogel.tex` and move it to `~/.pandoc/templates/`

```bash
cd <path to .md directory>
pandoc <.md file> -o <.pdf file> --from markdown --template eisvogel --listings
```

Fails with error `Could not find data file templates/eisvogel.latex`

```bash
mv ~/.pandoc/templates/eisvogel.tex  ~/.pandoc/templates/eisvogel.latex
pandoc <.md file> -o <.pdf file> --from markdown --template eisvogel --listings
```

Fails with error `pdflatex not found. Please select a different --pdf-engine or
install pdflatex`

```bash
sudo apt-get install texlive # This will be long
```

Fails :

```
Error producing PDF.
! LaTeX Error: File `csquotes.sty' not found.

Type X to quit or <RETURN> to proceed,
or enter new name. (Default extension: sty)

Enter file name:
! Emergency stop.
<read *>

l.176 ^^M
```

It wouldn't be great anyway, I can't use LaTeX for designing PDFs, I'd rather
CSS !

## Give a second chance to gimli

Maybe it is easy to swap coderay for something else.

Clone repo and search for highlight code

```bash
git clone https://github.com/walle/gimli.git
cd gimli
```

The `gimli` file in `bin` folder can be called directly with the same arguments
as before. The `coderay` code is in the file `lib/gimli/markup/code_block.rb`

### Use `rouge` instead of `coderay` 

`rouge` is a project in `GitHub` to highlight some text. It is ispired from
`Pygments`

```bash
sudo gem install rouge # Seems to work
```

Create a test project `mkdir /tmp/testsRouge; cd /tmp/testsRouge` and make some
tests.

The following code, in an executable file, print html code to the output:

```ruby
#!/usr/bin/env ruby

require 'rouge'

# make some nice lexed html
source = File.read('/home/hiesoo/.bashrc')
formatter = Rouge::Formatters::HTML.new
lexer = Rouge::Lexers::Shell.new
htmlText = formatter.format(lexer.lex(source))

puts htmlText
```

To test, we need to add this code in the HTML file :

```html
<!DOCTYPE html>
<html>
  <head>
    <link rel="stylesheet" href="file:///tmp/testsRouge/out.css" type="text/css" >
  </head>
  <body>
    <div class="highlight">
[... insert HTML code here ...]
    </div>
  </body>
</html>
```

The following code, in an executable file, print css code to the output:

```ruby
#!/usr/bin/env ruby

require 'rouge'

# Get some CSS
cssCode = Rouge::Themes::Base16.mode(:light).render(scope: '.highlight')
# Or use Theme#find with string input
# Rouge::Theme.find('base16.light').render(scope: '.highlight')

puts cssCode
```

`scope: '.highlight'` seems to be the css target for the html tag containing the
highlighted code.

There is no line break at the end of each line !

Also, there is no style for highlight code when using gimli to tranfsorm
markdown to html. Is it a default stylesheet to wkhtmltopdf arguments ?

> Yes ! it is in config/style.css

Check if HTMl classes generadted by `coderay` and those generated by `rouge` are
the same.

> No they aren't

Clone rouge and try to list all classes.

```bash
git clone https://github.com/jneen/rouge.git
cd rouge
```

The HTML classes are called `tokens`

We don't need to substitute the classes, we only need to get the `pygments`
style sheet and use it. Use the previous code to print CSS code to output with
`rouge`.

There is a project witch implements bash coloration for `coderay`:
https://github.com/pejuko/coderay_bash

### Try with `coderay_bash`

```bash
git clone https://github.com/pejuko/coderay_bash.git
```

Install coderay_bash

```bash
sudo gem install coderay_bash
```

Update gimli code to use `coderay_bash` instead of coderay. Edit file
`lib/gimli/markup/code_block.rb` : Replace the `require` line by the following :

```ruby
require 'rubygems'
require 'coderay_bash'
```

Try if it works :

```bash
cd ~/workspace/gimli/bin
./gimly -f ~/wordspace/documentations/markdown.md -debug
```

> There are classes for bash and the ruby code

```bash
cd ~/workspace/gimli/bin
./gimly -f ~/wordspace/documentations/markdown.md
```

Bash highlight is not great :( see an example :

```bash
cd $HOME
git clone url
./try something
```

![Bash highlight test](bash_highlight_test.png "highlist result for Bash")

It is better to not highlight bash ! This way we don't have to patch gimli

## Try to change design for the table of content

`-debug` option from gimli doesn't print the table of content

Try to use strace to see what gimli really calls

```bash
sudo apt-get install strace
strace gimli -f README.md -s style.css -w '-t' -o /tmp
```

> Prints a lot of traces

`wkhtmltopdf` manpage specify the `--dump-default-toc-xsl` option

```bash
wkhtmltopdf --dump-default-toc-xsl
```

```
Unknown long argument --dump-default-toc-xsl
```

Clone wkhtmltopdf and build it to have an up-to-date version

```bash
git clone https://github.com/wkhtmltopdf/wkhtmltopdf.git
cd wkhtmltopdf
sudo apt-get install qt5-qmake
qmake
```

> `qmake: could not find a Qt installation of ''`

The problem is probably my Qt installation. One day I'll fix it

