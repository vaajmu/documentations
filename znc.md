# Install and configure ZNC

## Installation

Download the source tarball at (https://znc.in/releases/).

Install prerequisites:

```bash
sudo apt-get install libssl-dev
```

Create a new user to run znc on it's own directory:

```bash
sudo useradd --create-home -d /var/lib/znc --system --shell /sbin/nologin --comment "Account to run ZNC daemon" --user-group znc
```

Build:

```bash
mkdir -p ~/workspace
cd ~/workspace
tar -xzvf ~/Downloads/znc-1.7.5.tar.gz
cd znc-1.7.5
./configure
make
sudo make install
```

## Configuration

```bash
sudo -u znc /usr/bin/znc --datadir=/var/lib/znc --makeconf
### Global settings
# Use port 6697 (standard for SSL)
# Enable SSL
# Use both IPv4 and IPv6
### Admin user settings
# The username can be the IRC nickname, used to connect to ZNC later
# The password will be used to connect to ZNC later
# Nick will be the IRC displayed name
# Alternate nick will be used if the nikname is already used
# Ident: ?? IRC Ident, usually your unix username, or anything you choose ??
# Real name can be a username or a real name (dosplayed on IRC)
# Bind host can be left empty
### Network settings
# Name: Freenode - human readable name for this network
# Server host: chat.freenode.net - server's domain name, IP, ...
# Use SSL: yes
# Server port: 6697 for Freenode with SSL
# Server password: used if the nickname is registered in the server (Freenode)
# Initial channels: #chan1, #chan2
```

At the end of the configuration, the program give an example on how to connect
to the ZNC server:

```
[ ** ] To connect to this ZNC you need to connect to it as your IRC server
[ ** ] using the port that you supplied.  You have to supply your login info
[ ** ] as the IRC server password like this: user/network:pass.
[ ** ]
[ ** ] Try something like this in your IRC client...
[ ** ] /server <znc_server_ip> +6697 anloh:<pass>
[ ** ]
[ ** ] To manage settings, users and networks, point your web browser to
[ ** ] https://<znc_server_ip>:6697/
```

Create a self signed key:

```bash
openssl req -x509 -newkey rsa:4096 -keyout key.pem -out cert.pem -days 365 -nodes
openssl dhparam -out dhparam.pem 2048

# print certificate data:
openssl x509 -in key.pem -text

cat key.pem > znc.pem
cat cert.pem >> znc.pem
sudo cp znc.pem /var/lib/znc/znc.pem

sudo systemctl restart znc.service
```

## Connect to the ZNC server

### General

* Username in the form 'username/network'
  * The username as choosed in the 'Admin user settings' section
  * '/'
  * The network name choosed in the 'Network settings' section
* Server: the ZNC server's IP
* Password: the one choosed in the 'Admin user settings' section
* Port: as choosen in the 'Global settings' section
* Use SSL:  as choosen in the 'Global settings' section

### Using weechat

Get the ZNC self signed certificate fingerprint

```bash
cat ~/.znc/znc.pem | openssl x509 -sha512 -fingerprint -noout | tr -d ':' | tr 'A-Z' 'a-z' | cut -d = -f 2
```

```
/server add BNC my.bouncer.net/6697 -ssl -username=username/network -password=password -autoconnect
/set irc.server.BNC.ssl_fingerprint MD5_or_SHA256_or_SHA512_fingerprint_here
/connect BNC
/save
```

## Update let's encrypt certificate

First, update the certificate created by the certbot tools:

```bash
sudo /usr/local/bin/certbot-auto renew
```

Once the Let's Encrypt certificate is up to date, we need to update the pem file
used by ZNC:

```bash
sudo su
cd /var/lib/znc/
cat /etc/letsencrypt/live/example.org/{privkey,fullchain}.pem > znc.pem
```

## Later configuration

Access the webinterface : `https://<server ip>:6697/`

Commands from IRC client:

```
# List connected cients:
/msg *clientbuffer listclients
```

### Store buffer per clients

It allow for each client (for the same account but on multiple devices) to get
every messages even if they where already delivered to another client.

From web interface, go to 'Your settings', click on '[Edit]' for the right
network, enable module clientbuffer and add 'autoadd' in the option field.

The autoadd option enables the clientbuffer for every client that
connects. Otherwise every client should register at least once.

### Store buffer on disk

It allow to play back buffers even after the ZNC server reboot.

From web interface, go to 'Your settings', click on '[Edit]' for the right
network, enable module savebuf and add a password in the option field.
