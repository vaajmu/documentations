# Register a private channel

```
# Configure channel as registered
/msg Chanserv register #my-canal

# Configure myself as admin
/msg Chanserv flags #my-canal my-nickname +F

# Configure channel as private
/msg Chanserv set #my-canal mlock +i

# In the channel
/mode #my-canal +I nick!user@host

# Fields can be jockers
/mode #my-canal +I my-friend!*@*

# Add another admin ?
/msg Chanserv flags #my-canal my-other-friend +vVRi

# Add another admin
/mode #my-canal +o my-friend!*@*
```

