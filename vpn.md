# Configure VPN

## On NixOs

### Install configuration

```bash
sudo mkdir /etc/nixos/openvpn/
sudo mv <openvpn file>.ovpn /etc/nixos/openvpn/
```

In NixOs configuration:

```nix
  services.openvpn.servers = {
    <conf1> = {
      config = ''config /etc/nixos/openvpn/<openvpn file1>.ovpn '';
      autoStart = false;
      updateResolvConf = true;
    };
    <conf2> = {
      config = ''config /etc/nixos/openvpn/<openvpn file2>.ovpn '';
      autoStart = false;
      updateResolvConf = true;
    };
```

### Store credentials

This is used for openvpn to not prompt the user for credentials when starting.

Create file `/etc/nixos/openvpn/login.conf`:

```
<login>
<password>
```

Make sure the file can only be read by the owner!

Edit file `config /etc/nixos/openvpn/<openvpn file1>.ovpn` and add:

```
auth-user-pass /etc/nixos/openvpn/login.conf
```

Make sure no other `auth-user-pass` rule is present on the file. Disable it if
there is one so it doesn't conflict with the new rule.

### Disable route for some domains or IPs

In case some domains or IPs should not use the VPN. For example local IPs like
`192.168.0.10`.

Edit file `config /etc/nixos/openvpn/<openvpn file1>.ovpn` and add:

```
# To disable routes only for IPs:
route 192.168.0.0/24 255.255.255.255 net_gateway

# To also disable domain names:
allow-pull-fqdn # Add this only once
route some.domain.com 255.255.255.255 net_gateway
```

Reload systemD configuration so it uses the updated configuration:

```
sudo systemctl daemon-reload
```

### Start & stop

```bash
sudo systemctl start <conf1>.service
sudo systemctl stop <conf1>.service
journalctl -xe -u <conf1>.service
```

